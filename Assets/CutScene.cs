﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CutScene : MonoBehaviour
{
    public string cutSceneName;
    public UnityEvent onFinishedCutScene;
    private void Awake()
    {
        if (PlayerPrefs.HasKey(cutSceneName))
        {
            onFinishedCutScene.Invoke();
        }
    }

    public void Skip()
    {
        PlayerPrefs.SetString(cutSceneName, "Done");
        onFinishedCutScene.Invoke();
    }
}
