﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RTLTMPro;

public class DisplayInformation : MonoBehaviour
{
 public TextMeshProUGUI descriptionText;
 public Image characterImage;
 public GameObject description;

 public GameObject[] images;

 private Vector3 _previousCameraPosition;
 private void OnEnable()
 {
  _previousCameraPosition = Camera.main.transform.position;
  Camera.main.transform.position = new Vector3(100, 100,100);
 }

 private void OnDisable()
 {
  Camera.main.transform.position = _previousCameraPosition;
 }


 public void ResetImage()
 {
  foreach (var image in images)
  {
   image.SetActive(false);
  }
 }
 
}
