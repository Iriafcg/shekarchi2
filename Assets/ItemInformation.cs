﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ItemInformation : MonoBehaviour
{
    public GameObject description;
    public GameObject targetImage;
    public void OnClick()
    {
        description.SetActive(true);
        targetImage.SetActive(true);
    }
}
