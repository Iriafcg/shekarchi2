﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JetEffectUI : MonoBehaviour
{
 private Image jet;

 private IEnumerator Start()
 {
  jet = GetComponent<Image>();
  
  float alpha = 0;
  while (alpha <= 1.0f)
  {
   alpha += 0.01f;
   jet.color = new Color (1, 1, 1, alpha);
   yield return null;

  }
  yield return null;

  while (alpha >= 0.0f)
  {
   alpha -= 0.01f;
   jet.color = new Color (1, 1, 1, alpha);
   yield return null;
  }

  StartCoroutine(Start());
 }
}
