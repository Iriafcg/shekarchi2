﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class HomingMissile : MonoBehaviour {


    public float speed = 5;
    public float rotatingSpeed= 200;
    public GameObject target;

    Rigidbody2D rb;
    private Vector3 direction;

    void Start () {

        rb = GetComponent<Rigidbody2D> ();
        var bombs = GameObject.FindGameObjectsWithTag("Bomb");
        var minDistance = bombs.Min(x => Vector3.Distance(transform.position,x.transform.position));
        target = bombs.FirstOrDefault(x => Vector3.Distance(transform.position, x.transform.position) <= minDistance);

    }

    public bool calculateOnce;
    void FixedUpdate () {
        if(target == null) return;
        
        if (target != null && !calculateOnce) 
        {
            direction = (target.transform.position - transform.position).normalized;
            calculateOnce = true;
        }

        transform.position += direction  * (speed * Time.deltaTime);

    }


    void OnTriggerEnter2D(Collider2D other)
    {

        // if(target == null) return;
        if (other.CompareTag("Bomb")) {
            transform.GetChild(0).transform.parent = null;
            Destroy(gameObject);
            Destroy(other.gameObject);
            calculateOnce = false;
        }
        //
        // if (!other.CompareTag("AntiMissle"))
        // {
        //     Destroy(gameObject);
        // }

    }
}