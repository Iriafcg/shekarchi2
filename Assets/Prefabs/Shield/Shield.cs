﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bomb"))
        {
            Destroy(other.gameObject);
        }
        
        if (other.CompareTag("EMP"))
        {
            Destroy(gameObject);
        }
    }

}
