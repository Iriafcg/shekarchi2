﻿using System;
using System.Collections;
using System.Collections.Generic;
using RTLTMPro;
using UnityEngine;
using UnityEngine.UI;

public class AirPlane : MonoBehaviour
{
    public Image backGroundColor;
    public Button buyButton;
    public CanvasGroup canvasGroup;
    public GameObject lockImage;
    public RTLTextMeshPro price;
    public int requiredStars;
    private int index;
    private Color _deActiveColor = new Color(1,0,0,0.5f);
    private Color _activeColor = new Color(0,0,1,0.5f);
    private Color _selectedColor = new Color(0,1,0,0.5f);
    [SerializeField] private AudioClip sound;

    private Shopping _shopping;

    private void Awake()
    {

        index = transform.GetSiblingIndex();
        price.text = requiredStars.ToString();
        _shopping = FindObjectOfType<Shopping>();

    }

    private void OnEnable()
    {
        Check();
    }

    private void OnValidate()
    {
        price.text = requiredStars.ToString();
    }

    public void Check()
    {
        var stars = PlayerPrefs.GetInt("Stars");
        if (requiredStars > stars)
        {
            buyButton.interactable = false;
            canvasGroup.interactable = false;
            lockImage.SetActive(true);
            DeActive();

        }
        else
        {
            lockImage.SetActive(false);
            canvasGroup.interactable = true;
            Active();
        }
        
        
    }

    public void Active()
    {
        backGroundColor.color =  _activeColor;
    }

    public void DeActive()
    {
        
        backGroundColor.color =  _deActiveColor;
    }


    public void Highlight()
    {
        backGroundColor.color =  _selectedColor;
    }

    public void Select()
    {
        AudioSource.PlayClipAtPoint(sound,Camera.main.transform.position);

        PlayerPrefs.SetInt("AirPlane",index);
        _shopping.SelectAirPlane(index);
        print(index);
    }
}
