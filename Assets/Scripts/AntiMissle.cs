﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiMissle : MonoBehaviour
{
    public GameObject prefab;
    public Transform ShotPos;
    

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(!other.CompareTag("Bomb")) return;
        var test = GameObject.FindGameObjectWithTag("Bomb");

        if (test != null)
        {
           var spawn = Instantiate(prefab, ShotPos);
        }
    }


    // void Update()
    // {
    //     if(IsShot) return;
    //     var directionVector = (target.position - transform.position).normalized;
    //
    //     Quaternion lookRotation = Quaternion.LookRotation(Vector3.forward, directionVector);
    //
    //     transform.rotation = Quaternion.Euler(0,0, lookRotation.eulerAngles.z);
    //
    //     transform.rotation = Quaternion.Euler(0,0, lookRotation.eulerAngles.z + 90);
    //
    //
    // }
}
