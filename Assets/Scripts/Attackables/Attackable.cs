﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Attackable : MonoBehaviour, IDamageable
{
	public int resistance;
    public Sprite damagedAppearance;
    public int score = 10000;
//	public int destructionSpeedThreshold = 7;
	public int damageSpeedThreshold = 2;

	[SerializeField]
	protected GameObject particlePrefabs;
	[SerializeField]
	protected GameObject scoreTextPrefab;

	protected ParticleSystem particles;
	protected Animation scoreTextAnim;

	protected Transform myTransform;
	protected SpriteRenderer spriteRenderer;
	private float hitPoints;

	public int Resistance
	{
		get
		{
			return resistance;
		}
	}

	public int Score
	{
		get
		{
			return score;
		}
	}

    protected virtual void Awake()
    {
		myTransform = transform;
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (particlePrefabs != null)
        {
	        particles = GameObject.Instantiate (particlePrefabs).GetComponent<ParticleSystem>();
	        particles.transform.position = transform.position;
	        particles.transform.parent = transform.parent;        
        }

        if (scoreTextPrefab != null)
        {
	        scoreTextAnim = GameObject.Instantiate (scoreTextPrefab).GetComponent<Animation>();
	        scoreTextAnim.transform.parent = transform.parent;
	        scoreTextAnim.GetComponent<TextMesh>().text = score.ToString();
	        hitPoints = (float)resistance;
        }
		//scoreTextAnim.transform.position = transform.position;

    }

    public virtual void ReceiveDamage(float amount)
    {
        hitPoints -= amount;

        if (hitPoints <= 0)
        {
            Destruction();
        }
        else
        {
            spriteRenderer.sprite = damagedAppearance;
        }

		AudioManager.PlaySound ("Collision");
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
//        if (collision.relativeVelocity.magnitude > destructionSpeedThreshold)
//        {
//            Destruction();
//        }
//        else
		if (collision.relativeVelocity.magnitude > damageSpeedThreshold)
        {
            ReceiveDamage(CollisionDamage(collision));
        }
    }

	protected float CollisionDamage(Collision2D collision)
	{
		// TODO: 5 is a placeholder. It should probably be calculated somehow.
		return 5;
	}

    public virtual void Destruction()
	{
		ScoreManager.Score += Score;

		scoreTextAnim.transform.position = transform.position + new Vector3 (0, Random.Range (0, 1f), 0);
		scoreTextAnim.transform.rotation = Quaternion.identity;
		scoreTextAnim.Play ();
		Destroy (scoreTextAnim.gameObject, scoreTextAnim.clip.length);

		particles.transform.position = transform.position;
		particles.transform.parent = null;
		particles.Play ();
		Destroy (particles.gameObject, particles.duration);
	}
}
