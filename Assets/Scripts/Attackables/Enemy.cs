using UnityEngine;
using System.Collections;
using Messaging;

public class Enemy : Attackable, ISubscriber {

	public bool isMale = true;
    Animator animator;
    new Collider2D collider;
    Rigidbody2D rigid;
	private string IdleAnimationName;

    protected override void Awake()
    {	
		base.Awake ();

		IdleAnimationName = name.Substring(0, name.IndexOf(" ") > 0 ? name.IndexOf(" ") : name.Length) + "_Idle_Right";

		animator = GetComponent<Animator>();
		if (null != animator)
		{
			Debug.Log("Playing animation <b>" + IdleAnimationName + "</b> from animator <b>" + name + "</b>");
			animator.Play(IdleAnimationName, 0, Random.Range(0, 1f));
		}
		collider = GetComponent<Collider2D>();
        rigid = GetComponent<Rigidbody2D>();
    }

	void Start()
	{
		SubscribeToEvent ("JetOut");
	}

    public override void Destruction()
    {
		base.Destruction ();
        
		MessageDispatcher.Instance.DispatchEvent (new EnemyDiedEvent());

		rigid.isKinematic = true;
        collider.enabled = false;
		if (isMale)
			AudioManager.PlaySound ("MaleEnemyDeath");
		else
			AudioManager.PlaySound ("FemaleEnemyDeath");

		Unsubscribe ("JetOut");
		Destroy (gameObject);
    }

	void Update()
	{
		if (animator.enabled && Vector3.Dot(rigid.transform.up, Vector3.up) <= .99f)
		{
			animator.enabled = false;
		}

		else if (!animator.enabled && Vector3.Dot(rigid.transform.up, Vector3.up) >= .99f)
		{
			animator.enabled = true;
		}
	}

	public class EnemyDiedEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "EnemyDied";
			}
		}
	}

	#region ISubscriber implementation

	public void SubscribeToEvent (string eventName)
	{
		MessageDispatcher.Instance.Subscribe(this, eventName);
	}

	public void Unsubscribe (string eventName)
	{
		MessageDispatcher.Instance.Unsubscribe(this, eventName);
	}

	public void PushEvent (HavEvent havEvent)
	{
		if (havEvent is Jet.JetOutEvent) 
			if (Random.Range (0, 3) == 0)  // 30%
				StartCoroutine (DelayBeforeLaugh (Random.Range (0, 2f)));
	}

	#endregion

	IEnumerator DelayBeforeLaugh (float delay)
	{
		yield return new WaitForSeconds (delay);
		AudioManager.PlaySound ("EnemyLaughing");
	}
}
