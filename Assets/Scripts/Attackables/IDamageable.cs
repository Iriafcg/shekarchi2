﻿using UnityEngine;
using System.Collections;

public interface IDamageable 
{
	void ReceiveDamage (float amount);
	void Destruction ();
	int Resistance {get;}
	int Score {get;}
}
