using UnityEngine;
using System.Collections;

public class Masaleh : Attackable
{

    public string materialName;

	public override void Destruction()
    {
		base.Destruction ();
		AudioManager.PlaySound (materialName);
        Destroy (gameObject);
	}

	public override void ReceiveDamage(float amount)
	{
		base.ReceiveDamage (amount);
		AudioManager.PlaySound (materialName);
	}
}
