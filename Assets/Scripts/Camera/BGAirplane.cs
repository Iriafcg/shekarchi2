﻿using UnityEngine;
using System.Collections;

public class BGAirplane : Parallax
{
	float downPos , upPos;

	void Start()
	{
		downPos = Random.Range (-10f, 10f);
		upPos = Random.Range (-5f, 0f);
	}

	void LateUpdate ()
	{
		temp = Mathf.Lerp (minScale, maxScale, Mathf.InverseLerp (3.25f, 12f, cam.orthographicSize));
		myTrans.localScale = new Vector3 (temp, temp, temp);

		temp = Mathf.Lerp (minPos, maxPos, Mathf.InverseLerp (3.25f, 12f, cam.orthographicSize));
		myTrans.localPosition = new Vector3 (myTrans.localPosition.x, initialY + temp, myTrans.localPosition.z);


		temp = Mathf.Lerp (maxSpeed, minSpeed, Mathf.InverseLerp (3.25f, 12f, cam.orthographicSize));
		myTrans.Translate (Vector3.right * Time.deltaTime * temp);

		temp = Mathf.Lerp (-20, 20, Mathf.InverseLerp (downPos, upPos, myTrans.localPosition.x));
		myTrans.Translate (Vector3.up * Time.deltaTime * temp);

		if (myTrans.localPosition.x > 15)
			Destroy (gameObject);
	}
}