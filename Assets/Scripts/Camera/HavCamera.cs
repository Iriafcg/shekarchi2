﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Automata;

public class HavCamera : StateMachine
{
	public Transform groundMarker;

	public Transform levelFocusMarker;
	public Transform levelBeginMarker;
	public Transform levelEndMarker;

	public Transform jetTransform;

	public float jetSpawnOffsetY = 1.7f;
	public float relativeFrameOffset = .4f;

	private float aspectRatio;
	private float levelGroundHeight;
	private float levelWidth;

	public float maxSize;
	public float minSize;
	private float manualSize;
	private bool hasManualSizeChanged  = false;

	new Camera camera;

	[HideInInspector]
	public Vector3 lastMousePosition;

	Init init;
	Follow catchUp;
	//SideShow seekJet;
	WaitForTarget waitForJet;
	StalkTarget stalkJet;
	SideShow seekBomb;
	Idle idle;
	ManualPanZoom manual;

	static HavCamera instance = null;

	public static HavCamera Instance
	{
		get
		{
			return instance;
		}
	}

	public void SetDefaultSize(float size)
	{
		if (size != minSize && size >= (levelBeginMarker.position.y - groundMarker.position.y) * .45f)
		{
			hasManualSizeChanged = true;
			manualSize = Mathf.Max(size, (levelBeginMarker.position.y - groundMarker.position.y) * .5f);
		}
	}

	protected override void Awake()
	{
		instance = this;

		base.Awake ();
		camera = Camera.main;

		manualSize = camera.orthographicSize;

		lastMousePosition = Input.mousePosition;

		if (!enabled) return;
		SetBoundaries ();

		init = new Init(this, "Cam+Init");
		catchUp = new Follow(this, "Cam+CatchUp");
		waitForJet = new WaitForTarget(this, "Cam+WaitForJet");
		stalkJet = new StalkTarget(this, "Cam+StalkJet");
		seekBomb = new SideShow(this, "Cam+SeekBomb");
		idle = new Idle(this);
		manual = new ManualPanZoom(this);

		InitialState = init;
//		AddState (init);
//		AddState (catchUp);
//		AddState (seekJet);
//		AddState (seekBomb);
//		AddState (manual);
//		AddState (idle);

		AddTransition (init, catchUp, "BombEquipped");
		AddTransition (catchUp, waitForJet, "TargetReached");
		AddTransition (waitForJet, stalkJet, "TargetVisualized");
		AddTransition (waitForJet, seekBomb, "BombReleased");
		AddTransition (stalkJet, seekBomb, "BombReleased");
		AddTransition (manual, seekBomb, "BombReleased");

		AddTransition (seekBomb, init, "NextTurn");
		AddTransition (stalkJet, init, "NextTurn");

		AddTransition (idle, catchUp, "BombEquipped");
		AddTransition (idle, seekBomb, "BombReleased");
		AddTransition (idle, init, "NextTurn");

		#region Input related state transtitions

		AddTransition (init, manual, "MouseDrag");
		//AddTransition (catchUp, manual, "MouseDrag");
		AddTransition (stalkJet, manual, "MouseDrag");
		AddTransition (waitForJet, manual, "MouseDrag");
		AddTransition (seekBomb, manual, "MouseDrag");
		AddTransition (idle, manual, "MouseDrag");

		AddTransition (manual, manual, "MouseDrag");
		AddTransition (manual, idle, "MouseDragDone");

		#region Touch input related state transitions

		AddTransition (init, manual, "TouchSwiping");
		//AddTransition (catchUp, manual, "TouchSwiping");
		AddTransition (stalkJet, manual, "TouchSwiping");
		AddTransition (waitForJet, manual, "TouchSwiping");
		AddTransition (seekBomb, manual, "TouchSwiping");
		AddTransition (idle, manual, "TouchSwiping");


		AddTransition (init, manual, "TouchPinching");
		//AddTransition (catchUp, manual, "TouchPinching");
		AddTransition (stalkJet, manual, "TouchPinching");
		AddTransition (waitForJet, manual, "TouchPinching");
		AddTransition (seekBomb, manual, "TouchPinching");
		AddTransition (idle, manual, "TouchPinching");

		AddTransition (manual, manual, "TouchPinching");
		AddTransition (manual, manual, "TouchSwiping");
		AddTransition (manual, idle, "TouchDone");

		AddTransition (idle, idle, "MouseClick");
		AddTransition (manual, idle, "MouseClick");

		#endregion

		#endregion

		AddTransition (seekBomb, init, "BombExploded");
		AddTransition (stalkJet, init, "JetOut");

		AddTransition (manual, catchUp, "BombEquipped");
		AddTransition (manual, init, "NextTurn");
		AddTransition (manual, manual, "JetOut");

		AddTransition(init, idle, "TargetReached");
		AddTransition(seekBomb, idle, "TargetReached");
		AddTransition(stalkJet, idle, "TargetReached");

	}

	protected override void Start ()
	{
		base.Start ();

		SubscribeToEvent ("BombEquipped");
		SubscribeToEvent ("BombReleased");
		SubscribeToEvent ("NextTurn");
		SubscribeToEvent ("MouseDrag");
		SubscribeToEvent ("MouseDragDone");
		SubscribeToEvent ("MouseClick");
		SubscribeToEvent ("TouchSwiping");
		SubscribeToEvent ("TouchPinching");
		SubscribeToEvent ("TouchDone");
		SubscribeToEvent ("BombExploded");
		SubscribeToEvent ("JetOut");

		init.Target = levelFocusMarker;

		catchUp.Target = levelBeginMarker;
		catchUp.seekInterval = .5f;
		catchUp.waitOnReach = .5f;

		waitForJet.Target = jetTransform;
		stalkJet.Target = jetTransform;

		seekBomb.Target = levelFocusMarker;
		seekBomb.trackSpeed = .33f;
	}

	private void SetBoundaries()
	{
		aspectRatio = (float)Screen.width / (float)Screen.height;

		levelGroundHeight = levelFocusMarker.position.y - groundMarker.position.y;
		levelWidth = levelEndMarker.position.x - levelBeginMarker.position.x;

		maxSize = (levelWidth / aspectRatio) / 2;
		minSize = levelGroundHeight / 2;
	}

	public override void Update ()
	{
		lastMousePosition = Input.mousePosition;
		base.Update ();
		EnforceZoomBoundaries();
		EnforcePanBoundaries();
	}

	void EnforceZoomBoundaries ()
	{
		camera.orthographicSize = EnforceZoomBoundaries (camera.orthographicSize);
	}

	float EnforceZoomBoundaries (float size)
	{
		float max = hasManualSizeChanged ? Mathf.Min(maxSize, manualSize) : maxSize;
		float min = hasManualSizeChanged ? Mathf.Max(minSize, manualSize) : minSize;

		if (size > max)
			return max;
		else if (size < min)
			return min;
		else
			return size;
	}

	void EnforcePanBoundaries ()
	{
		camera.transform.position = EnforcePanBoundaries(camera.transform.position, camera.orthographicSize);
	}

	Vector3 EnforcePanBoundaries (Vector3 position, float size)
	{
		var camPosClone = position;
		if (position.x > levelEndMarker.position.x - size * aspectRatio)
		{
			camPosClone.x = levelEndMarker.position.x - size * aspectRatio;
		}
		if (position.x < levelBeginMarker.position.x + size * aspectRatio)
		{
			camPosClone.x = levelBeginMarker.position.x + size * aspectRatio;
		}
		camPosClone.y = groundMarker.position.y + size;
		return camPosClone;
	}

	public override void PushEvent (HavEvent havEvent)
	{
		if ("Cam+CatchUp" == currentState.Name && havEvent is TargetReachedEvent)
		{
			DispatchEvent(new SendJetInEvent());
		}

		base.PushEvent (havEvent);
	}

	void SetMinSizeToSpawnPoint()
	{
		minSize = (levelBeginMarker.position.y - groundMarker.position.y) * .5f;
	}

	public float offset;
	void SetMinSizeToFocus()
	{
		minSize = (levelFocusMarker.position.y - groundMarker.position.y) * .5f;
		minSize += offset;
		maxSize += offset;
	}

	#region States for the FSM

	class Idle : PanZoom
	{
		float driftThreshold = 0f;

		public Idle (StateMachine owner) : base (owner)
		{
		}

		public override void OnEnter (HavEvent havEvent)
		{
			base.OnEnter(havEvent);
			if (havEvent is InputHandler.MouseClickEvent)
			{
				isPanning = false;
			}
			#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_STANDALONE
			else if ( InputHandler.Instance.buttons[0].wasDownLastFrame)
			{
				panSpeed = 2;
				movedUnits.x = (InputHandler.Instance.buttons[0].origin - Input.mousePosition).x;
				if (Mathf.Abs(movedUnits.x) >= driftThreshold)
				{
					isPanning = true;
				}
			}
			#elif UNITY_ANDROID
			else if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				panSpeed = 2;
				movedUnits.x = -InputHandler.Instance.TouchDelta.x;
				if (Mathf.Abs(movedUnits.x) >= driftThreshold)
				{
					isPanning = true;
				}
//				else
//					CameraInternalStateMonitor.Instance.AddEntry("TOUCH DELTA NOT LARGER THAN THRESHOLD");
			}
//			else
//				CameraInternalStateMonitor.Instance.AddEntry("TOUCH PHASE NOT ENDED");
			#endif

		}

		public override void OnLeave (HavEvent havEvent)
		{
			isPanning = false;
		}

		protected override void SetPanAndZoomFlags (HavEvent havEvent) {}
		protected override void Zoom () {}
	}

//	private class TwoStageSideShow : SideShow
//	{
//		/*
//		 *  1. Wait until thing is a third of the frame into the frame
//		 *  2. Move camera as if glued onto thing
//		 *  3. Realize these are two separate states and implement them independently
//		 */
//	}

	private class WaitForTarget : State
	{
		//public float relativeFrameOffset = .4f;
		public Transform Target {get;set;}
		private float breakPoint;
		private Camera camera;

		public WaitForTarget(StateMachine owner) : base(owner) {camera = Camera.main;}
		public WaitForTarget(StateMachine owner, string name) : base(owner, name) {camera = Camera.main;}
		public WaitForTarget() : base() {camera = Camera.main;}

		public override void OnEnter (HavEvent havEvent)
		{
			((HavCamera)stateMachine).SetMinSizeToSpawnPoint();
			breakPoint = camera.orthographicSize * ((HavCamera)stateMachine).relativeFrameOffset * Screen.width / Screen.height;
		}

		public override void Update ()
		{
			if (Target.position.x >= (camera.transform.position.x - breakPoint))
			{
				PushEvent(new TargetVisualizedEvent());
			}
		}
	}

	private class StalkTarget : State
	{
		public Transform Target {get;set;}
		//public float relativeFrameOffset = .4f;

		private Vector3 tempPosition;
		float stalkPoint;
		private Camera camera;

		public StalkTarget(StateMachine owner) : base(owner) {camera = Camera.main;}
		public StalkTarget(StateMachine owner, string name) : base(owner, name) {camera = Camera.main;}
		public StalkTarget() : base() {camera = Camera.main;}

		public override void OnEnter (HavEvent havEvent)
		{
			stalkPoint = ((HavCamera)stateMachine).relativeFrameOffset * camera.orthographicSize * Screen.width / Screen.height;
		}

		public override void Update ()
		{
			tempPosition = camera.transform.position;
			tempPosition.x = Target.position.x + stalkPoint;
			camera.transform.position = tempPosition;
		}
	}

	private class SideShow : Follow
	{
		public float trackSpeed = 2f;
		private List<Transform> sideShows;
		private float maxTolerance = 10.0f;

		public void AddSideShow(Transform sideShow)
		{
			sideShows.Add(sideShow);
		}

		public float ManualZoom {get;set;}

		public SideShow(StateMachine owner, string name) : base (owner, name)
		{
			sideShows = new List<Transform>();
		}

		public SideShow(StateMachine owner) : base (owner)
		{
			sideShows = new List<Transform>();
		}

		protected override void Zoom ()
		{
			if (0 == sideShows.Count)
				return;

			float maxX = ((HavCamera)stateMachine).levelBeginMarker.position.x
				, maxY = ((HavCamera)stateMachine).groundMarker.position.y
				, minX = ((HavCamera)stateMachine).levelEndMarker.position.x;
			Vector3 pos;
			for (int i = 0; i < sideShows.Count; ++i)
			{
				if (null != sideShows[i])
				{
					//Debug.DrawRay(sideShows[i].position, new Vector3(1, 1, 0), Color.cyan, 1000, false);
					pos = sideShows[i].position;
					if (pos.x < minX)
						minX = pos.x;
					if (pos.x > maxX)
						maxX = pos.x;
					if (pos.y > maxY)
						maxY = pos.y;
				}
			}

			// TODO: THESE NAMES ARE HORRIBLE + so many magic numbers and "unnamed variables"
			// TODO: This works well for one sideshow. More needs to take into account the fact that some items may be leading and some lagging.
			var width = Mathf.Abs(maxX - camera.transform.position.x);
			var camWidth = camera.orthographicSize * Screen.width / Screen.height;
			var horizontalDefault = ManualZoom * Screen.width / Screen.height;

			if (width > camWidth)
			{
				camera.orthographicSize += (width - camWidth) * 1.5f * Screen.width / Screen.height * trackSpeed * Time.deltaTime;
			}

			if (width < camWidth && horizontalDefault < camWidth)
			{
				camera.orthographicSize += (horizontalDefault - camWidth) * 1.5f * Screen.width / Screen.height * trackSpeed * Time.deltaTime;
			}

			var maxHeight = maxY - ((HavCamera)stateMachine).groundMarker.position.y;

			if (maxHeight > 2 * camera.orthographicSize)
			{
				camera.orthographicSize += (maxHeight * .5f * trackSpeed)* Time.deltaTime;
			}

			if (maxHeight < 2 * camera.orthographicSize && maxHeight > 2 * ManualZoom)
			{
				camera.orthographicSize += (ManualZoom - camera.orthographicSize) * trackSpeed * Time.deltaTime;
			}

			if (camera.orthographicSize > ManualZoom + maxTolerance)
				camera.orthographicSize = ManualZoom + maxTolerance;
			//base.Zoom ();
		}

		public override void OnEnter (HavEvent havEvent)
		{
			base.OnEnter (havEvent);

			((HavCamera)stateMachine).SetMinSizeToFocus();

			if (havEvent is GameController.BombReleasedEvent)
			{
				if (null != ((GameController.BombReleasedEvent)havEvent).Target)
				{
					sideShows.Add(((GameController.BombReleasedEvent)havEvent).Target);
				}
			}
//			else if (havEvent is BombBase.BombExplodedEvent)
//			{
//				sideShows.Clear();
//			}

			ManualZoom = camera.orthographicSize;
		}

		public override void OnLeave (HavEvent havEvent)
		{
			sideShows.Clear();
		}
	}

	private class Init : Follow
	{
		public Init (StateMachine owner, string name) : base (owner) {Name = name;}
		public Init (StateMachine owner) : base (owner) {}
		public Init () {}

		public override void OnEnter (HavEvent havEvent)
		{
			base.OnEnter(havEvent);
			((HavCamera)stateMachine).SetMinSizeToFocus();
		}
	}

	private class Follow : PanZoom
	{
		public float zoomTolerance = .1f;
		public float maxOffset = 0.17f;	// TODO: MAGIC NUMBER HERE
		public bool minSizeLimited = false;

		private Transform seekTarget;
		public float seekInterval = .75f;
		public float waitOnReach = 0;

		private float activationTime;

		private float positionDiff = 0;
		private float zoomDiff = 0;
		float initTime = 0;
		float deltaTime = 0;

		float minZoom;

		Vector3 boundTargetPosition;
		float targetZoom;

		public Follow (StateMachine owner, string name) : base (owner)
		{
			Name = name;
		}

		public Follow(StateMachine owner) : base (owner)
		{
		}

		public Follow()
		{
		}

		public Transform Target
		{
			set
			{
				seekTarget = value;
				ResetParams ();
			}
		}

		public override void OnEnter (HavEvent havEvent)
		{
			initTime = Time.time;
			base.OnEnter (havEvent);
			ResetParams ();
			panSpeed = 1;
			zoomSpeed = 1;
		}

		void ResetParams ()
		{
			isPanning = true;
			isZooming = true;
			UpdateDistances();
		}

		void UpdateDistances()
		{
			if (null != seekTarget)
			{
				var owner = (HavCamera)stateMachine;
				targetZoom = owner.EnforceZoomBoundaries((seekTarget.position.y - owner.groundMarker.position.y) * .5f);
				zoomDiff = targetZoom - camera.orthographicSize;
				boundTargetPosition = owner.EnforcePanBoundaries(seekTarget.position, targetZoom);
				positionDiff = boundTargetPosition.x - camera.transform.position.x;
				owner.DrawCross(boundTargetPosition, targetZoom);
			}
		}

		public override void OnLeave (HavEvent havEvent)
		{
			isPanning = false;
			isZooming = false;
		}

		protected override void Zoom ()
		{
			movedUnits.y = Mathf.Lerp(0, zoomDiff, deltaTime / seekInterval);//zoomDiff * Time.deltaTime / seekInterval;
			base.Zoom ();
		}

		protected override void Pan ()
		{
			//movedUnits.x = positionDiff /** Time.deltaTime*/ / seekInterval;
			movedUnits.x = Mathf.Lerp(0, positionDiff, deltaTime / seekInterval);
			base.Pan ();
		}

		protected override void SetPanAndZoomFlags (HavEvent havEvent)
		{
			//if (Time.time >= seekTimer + waitOnReach)
			if (Mathf.Abs(camera.orthographicSize - targetZoom) < zoomTolerance)
			{
				isZooming = false;
			}
//			else
//			{
//				Debug.Log("<color=red>Zoom error: </color> <b>" + Mathf.Abs(camera.orthographicSize - targetZoom) + " </b>");
//			}


			if (Mathf.Abs(camera.transform.position.x - boundTargetPosition.x) < maxOffset)
			{
				isPanning = false;
			}
//			else
//			{
//				Debug.Log("<color=red>Pos error: </color> <b>" + Mathf.Abs(camera.transform.position.x - boundTargetPosition.x) + " </b>");
//			}

			if (!isPanning && !isZooming)
			{
				PushEvent (new TargetReachedEvent());
			}
		}

		public override void Update ()
		{
			deltaTime = Time.time - initTime;
			((HavCamera)stateMachine).DrawCross(boundTargetPosition, targetZoom);
			base.Update ();
			//seekInterval -= Time.deltaTime;
			//UpdateDistances();
		}
	}

	private class ManualPanZoom : PanZoom
	{
		public float touchPanSpeed = 0.1f;
		public float touchZoomSpeed = 0.02f;

		public float touchPanScale = 4f;
		public float touchZoomScale = 1.02f;

		private Vector3 mousePosition;
		private Vector3 panVector = new Vector3(0, 0, 0);

		public ManualPanZoom (StateMachine owner) : base (owner)
		{
		}

		public override void OnEnter (HavEvent havEvent)
		{
			base.OnEnter (havEvent);

			if (havEvent is Jet.JetOutEvent)
			{
				((HavCamera)stateMachine).SetMinSizeToFocus();
			}

			SetPanAndZoomFlags (havEvent);

			((HavCamera)stateMachine).hasManualSizeChanged = false;
			zoomSpeed = 800;
			panSpeed = 360;
		}
		public override void OnLeave (HavEvent havEvent)
		{
			//CameraInternalStateMonitor.Instance.AddEntry("EVENT+" + havEvent);
			isPanning = false;
			if (camera.orthographicSize > 
				(((HavCamera)stateMachine).levelBeginMarker.position.y - ((HavCamera)stateMachine).groundMarker.position.y) * .5f)
			{
				((HavCamera)stateMachine).hasManualSizeChanged = true;
				((HavCamera)stateMachine).manualSize = camera.orthographicSize;
			}
		}

		protected override void Zoom ()
		{
			#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_STANDALONE
			movedUnits.y = camera.ScreenToViewportPoint (Input.mousePosition - mouseOrigin).y;
			base.Zoom ();

			#elif UNITY_ANDROID

			Touch touch1 = Input.GetTouch(0);
			Touch touch2 = Input.GetTouch(1);

			if (TouchPhase.Moved != touch1.phase && TouchPhase.Moved != touch2.phase)
				return;

			Vector3 touch1WorldNew = camera.ScreenToWorldPoint(touch1.position);
			Vector3 touch2WorldNew = camera.ScreenToWorldPoint(touch2.position);
			float touchDeltaNew = (touch1WorldNew - touch2WorldNew).magnitude;

			Vector3 touch1WorldOld = camera.ScreenToWorldPoint(touch1.position - touch1.deltaPosition);
			Vector3 touch2WorldOld = camera.ScreenToWorldPoint(touch2.position - touch2.deltaPosition);
			float touchDeltaOld = (touch1WorldOld - touch2WorldOld).magnitude;

			float zoomScale = touchDeltaOld / touchDeltaNew;
			if (zoomScale > 1)
				zoomScale *= touchZoomScale;
			else
				zoomScale /= touchZoomScale;
			camera.orthographicSize *= zoomScale;

			#endif
		}

		protected override void Pan ()
		{
			#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_STANDALONE

			var originWorldCoords = camera.ScreenToWorldPoint(mouseOrigin);
			var positionWorldCoords = camera.ScreenToWorldPoint(mousePosition);

			panVector.y = panVector.z = 0;
			panVector.x = originWorldCoords.x - positionWorldCoords.x;

			camera.transform.Translate(panVector);

			mouseOrigin.x = Input.mousePosition.x;

			#elif UNITY_ANDROID

			mousePosition = Input.GetTouch(0).position;

			Vector3 originWorldCoords = camera.ScreenToWorldPoint(mousePosition - (Vector3)Input.GetTouch(0).deltaPosition);
			Vector3 positionWorldCoords = camera.ScreenToWorldPoint(mousePosition);

			panVector.x = (positionWorldCoords - originWorldCoords).x * touchPanScale;
			panVector.y = panVector.z = 0;

			camera.transform.Translate(-panVector);
			//CameraInternalStateMonitor.Instance.AddEntry("Touch at: " + mousePosition);


			mouseOrigin.x = mousePosition.x;

			#endif 
		}

		public override void Update ()
		{
			if (isPanning)
			{
				mouseOrigin.x = InputHandler.Instance.buttons[0].origin.x;
				mousePosition.x = Input.mousePosition.x;
			}

			if (isZooming)
			{
				mouseOrigin.y = InputHandler.Instance.buttons[1].origin.y;
				mousePosition.y = Input.mousePosition.y;
			}

			base.Update ();
		}

		protected override void SetPanAndZoomFlags (HavEvent havEvent)
		{
//			if (null == havEvent)
//				return;
			//CameraInternalStateMonitor.Instance.AddEntry("EVENT + " + havEvent);
			#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_STANDALONE

			if (havEvent is InputHandler.MouseDragEvent)
			{
				if ((havEvent as InputHandler.MouseDragEvent).Index == 0)
					isPanning = true;
			}
			else if (havEvent is InputHandler.MouseDragDoneEvent)
			{
				if ((havEvent as InputHandler.MouseDragDoneEvent).Index == 0)
					isPanning = false;
			}

			if (havEvent is InputHandler.MouseDragEvent)
			{
				if ((havEvent as InputHandler.MouseDragEvent).Index == 1)
					isZooming = true;
			}
			else if (havEvent is InputHandler.MouseDragDoneEvent)
			{
				if ((havEvent as InputHandler.MouseDragDoneEvent).Index == 1)
					isZooming = false;
			}

			#elif UNITY_ANDROID

			if (havEvent is InputHandler.TouchSwipingEvent)
			{
				isPanning = true;
				//CameraInternalStateMonitor.Instance.AddEntry("PANNING + " + havEvent);
			}
			#endif
		}
	}

	private abstract class PanZoom : State
	{
		protected Camera camera;

		protected Vector3 mouseOrigin;
		public float zoomSpeed = .5f;
		public float panSpeed = 180.0f;

		protected bool isPanning;
		//private bool isRotating;
		protected bool isZooming;

		protected Vector3 movedUnits = new Vector3();

		public PanZoom(StateMachine owner) : base(owner)
		{
			SetDefaults();
		}

		public PanZoom()
		{
			SetDefaults();
		}

		public void SetDefaults()
		{
			camera = Camera.main;
		}

		public override void OnEnter (HavEvent havEvent)
		{
			isPanning = false;
			isZooming = false;
		}

		protected abstract void SetPanAndZoomFlags (HavEvent havEvent = null);

		public override void Update ()
		{
			SetPanAndZoomFlags ();

			if (isZooming)
			{
				Zoom ();
			}

			if (isPanning)
			{
				Pan ();
			}
		}

		protected virtual void Zoom ()
		{
			camera.orthographicSize += movedUnits.y * zoomSpeed * Time.deltaTime;
		}

		protected virtual void Pan ()
		{
			Vector3 translation = new Vector3 (movedUnits.x * panSpeed, 0, 0);
			camera.transform.Translate (translation * Time.deltaTime);
		}
	}
	#endregion

	#region FSM Events

	class IdleTimeOutEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "IdleTimeOut";
			}
		}
	}

	class TargetReachedEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "TargetReached";
			}
		}
	}

	public class SendJetInEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "SendJetIn";
			}
		}
	}

	public class TargetVisualizedEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "TargetVisualized";
			}
		}
	}

	#endregion

	#region Debug UI
	void DrawCross (Vector3 position, float scale)
	{
		Debug.DrawRay (position, new Vector2 (scale, scale), Color.red);
		Debug.DrawRay (position, new Vector2 (-scale, -scale), Color.red);
		Debug.DrawRay (position, new Vector2 (scale, -scale), Color.red);
		Debug.DrawRay (position, new Vector2 (-scale, scale), Color.red);

		Debug.DrawLine (position, levelBeginMarker.position, Color.green);
		Debug.DrawLine (position, levelEndMarker.position, Color.blue);
		Debug.DrawLine (position, levelFocusMarker.position, Color.yellow);

		Debug.DrawRay(groundMarker.position, scale * Vector2.up, Color.yellow);
	}
	#endregion
}
