﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

	public bool moving = true;
	public float speedFactor = 1;

	protected float minScale , maxScale, minPos, maxPos, minSpeed, maxSpeed;

	protected float zDepth;

	protected Camera cam;
	protected Transform myTrans;

	protected float initialX , cameraOffsetX;
	protected float temp,initialY;

	public void Awake () 
	{
		cam = Camera.main;
		cameraOffsetX = cam.transform.position.x;
		myTrans = transform;
		initialX = myTrans.position.x;
		// TODO: SO MANY MAGIC NUMBERS! WHAT DO THEY ALL MEAN?
		zDepth = Random.Range (myTrans.localPosition.z, myTrans.localPosition.z + 0.5f);
		initialY = myTrans.localPosition.y;
		minScale  = 0.4f + 1 / Mathf.Abs(zDepth);
		maxScale = 0.6f - 1 / Mathf.Abs(zDepth);
		minPos = 0.5f / Mathf.Abs(zDepth);
		maxPos = - 1f / Mathf.Abs(zDepth);
		minSpeed = (0.5f + zDepth / 2) * speedFactor;
		maxSpeed = (3 + zDepth / 2) * speedFactor;
	}

	void LateUpdate () 
	{
		temp = Mathf.Lerp (minScale , maxScale , Mathf.InverseLerp(5f , 11f , cam.orthographicSize));
		myTrans.localScale = new Vector3(temp , temp, temp);

		temp = Mathf.Lerp (minPos , maxPos , Mathf.InverseLerp(5f , 11f , cam.orthographicSize));
		myTrans.localPosition = new Vector3(myTrans.localPosition.x , initialY + temp, myTrans.localPosition.z);

		if (moving)
		{
			temp = Mathf.Lerp (maxSpeed, minSpeed, Mathf.InverseLerp (5f, 11f, cam.orthographicSize));
			myTrans.Translate (Vector3.right * Time.deltaTime * temp);

			if (myTrans.localPosition.x > 15)
				Destroy (gameObject);
		} 
		else 
		{
			var parentPos = (cam.transform.position.x - cameraOffsetX) * (myTrans.position.z / 10) + initialX;
			myTrans.position = new Vector3 (parentPos, myTrans.position.y, myTrans.position.z);
		}
	}
}
