﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
	float timer = 2f;
	float endTime;

	// Use this for initialization
	void Start ()
	{	
		endTime = Time.time + timer;
	}
	
	// Update is called once per frame
	public void LoadMenu ()
	{
		SceneManager.LoadScene("Menu");	// Scene 2 is the menu
	}
}
