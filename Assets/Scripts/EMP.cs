﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMP : BombNormal
{
    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("AntiMissle") || other.CompareTag("Enemy")) return;
        
        
        var material = other.GetComponent<Masaleh>();
        if(material != null && material.materialName == "Glass"){
            OnExplosiveCollision (other.gameObject);
            return;
        }

        Instantiate(explosionEffectPrefab,transform.position,Quaternion.identity);
        // base.OnTriggerEnter2D(other);
        //
        Explode (true,"zap");   

    }
}
