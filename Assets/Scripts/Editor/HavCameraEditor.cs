﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(HavCamera))]
public class HavCameraEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		serializedObject.Update();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
