﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeNeighbour : MonoBehaviour
{
    public LayerMask destroyable;
    public float explosionRadius = 5;

    private void OnDestroy()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius, destroyable);
        for (int i = 0; i < colliders.Length; ++i)
        {
            var hit = colliders[i].gameObject;
            if (hit.gameObject == this.gameObject)
                continue;
            if (hit.GetComponent<Explosive>() != null)
            {
                hit.GetComponent<Explosive>().Explode();
            }
        }
    }
}
