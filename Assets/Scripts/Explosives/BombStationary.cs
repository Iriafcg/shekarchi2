﻿using UnityEngine;
using System.Collections;

public class BombStationary : Explosive, IDamageable
{
	public float speedThreshold;
	public int resistance;
	public int score;
	float hp;

	public int Resistance
	{
		get
		{
			return resistance;
		}
	}

	public int Score
	{
		get
		{
			return score;
		}
	}

	protected override void Awake ()
	{
		base.Awake ();
		hp = resistance;
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.relativeVelocity.magnitude  > speedThreshold)
		{
			Explode ();
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (null != other.gameObject.GetComponent<IShootable>())
		{
			Explode ();
		}
	}

	protected override void OnDestroy()
	{
		base.OnDestroy ();
		Explode ();
		PlayExplosionEffects ();
		Destruction ();
	}

	#region IDamageable implementation

	public void ReceiveDamage (float amount)
	{
		hp -= amount;
		if (hp <= 0)
		{
			Destruction ();
		}
	}

	public void Destruction ()
	{
		Explode ();
	}

	#endregion
}
