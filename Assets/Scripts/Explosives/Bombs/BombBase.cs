﻿using UnityEngine;
using System.Collections;
using Messaging;

public abstract class BombBase : Explosive, IShootable {
	

	[SerializeField]
	protected float shootVelocity = 7;

	public GameObject trailPrefab = null;
	protected GameObject trail;

	private AudioSource audioSource;

	float mastMalFactor = 3.2f;

	public Sprite image;
	public Sprite ButtonUI 
	{
		get
		{
			return image;
		}
	}

	protected override void Awake()
	{
		base.Awake ();
		if (null != trailPrefab)
		{
			trail = GameObject.Instantiate (trailPrefab);
			trail.transform.position = this.transform.position;
			trail.transform.parent = this.transform;
			trail.SetActive(false);
		}
		audioSource = GetComponent<AudioSource> ();
		if (PlayerPrefs.GetInt ("AudioOption", 1) == 0)
		{
			if (null != audioSource)
			{
				audioSource.mute = true;
			}
		}
	}

	public virtual void Shoot()
	{
		rigid.isKinematic = false;
		if (audioSource != null)
			audioSource.Play ();

		rigid.velocity =  Vector2.right * shootVelocity;
		BombTrajectory.Display (false , Vector3.zero );

		if (null != trail)
			trail.SetActive(true);
	}

	protected virtual void FixedUpdate()
	{
		Vector3 forward = rigid.velocity;
		var up = Vector3.Cross (forward, Vector3.back);
		rigid.rotation = -Quaternion.LookRotation(Vector3.back, up).eulerAngles.z;
	}

	protected override void OnDestroy()
	{
		if (null != trail)
		{
			trail.transform.parent = null;
		}
		base.OnDestroy ();
		MessageDispatcher.Instance.DispatchEvent(new BombExplodedEvent());
	}

	public virtual Transform Position
	{
		get
		{
			return gameObject.transform;
		}
	}

	public virtual void Equip (bool showTrajectory)
	{
		if (showTrajectory)
			BombTrajectory.Display (true , (shootVelocity / mastMalFactor) * Vector3.right );		
	}

	protected virtual void OnCollisionEnter2D(Collision2D collision)
	{
		var enemy = collision.collider.gameObject.GetComponent<Enemy> ();
		if (null != enemy)
		{
			enemy.Destruction();
		}
	}

	protected virtual void OnTriggerEnter2D(Collider2D collider)
	{
		var enemy = collider.gameObject.GetComponent<Enemy> ();
		if (null != enemy)
		{
			enemy.Destruction();
		}
	}

	public class BombExplodedEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "BombExploded";
			}
		}		
	}
}
