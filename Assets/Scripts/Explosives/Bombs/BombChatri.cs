﻿using UnityEngine;
using System.Collections;
using Messaging;

public class BombChatri : BombBase, ISubscriber
{
	private bool isShot = false;
	private bool preShot = false;
	private Vector3 mouseOrigin;

	void OnTriggerEnter2D(Collider2D other)
    {
	    if (other.CompareTag("AntiMissle")) return;

		if (null != other.GetComponent<Enemy>())
		{
			other.GetComponent<Enemy>().Destruction();
		}
		else
		{
			OnExplosiveCollision (other.gameObject);
		}

		var material = other.GetComponent<Masaleh>();
		if (material != null && material.materialName == "Glass") return;

		Explode ();
    }

	public override void Equip (bool showTrajectory)
	{
		base.Equip (showTrajectory);
	}

	public override void Shoot ()
	{
		base.Shoot ();
		SubscribeToEvent("MouseClick");
		preShot = true;
	}

	protected override void FixedUpdate ()
	{
		base.FixedUpdate ();
		rigid.rotation += 90;
		if (preShot)
		{
			isShot = true;
			preShot = false;
		}
	}

	protected override void OnDestroy ()
	{
		Unsubscribe("MouseClick");
		base.OnDestroy ();
	}

	public void SubscribeToEvent (string eventName)
	{
		MessageDispatcher.Instance.Subscribe(this, eventName);
	}

	public void Unsubscribe (string eventName)
	{
		MessageDispatcher.Instance.Unsubscribe(this, eventName);
	}

	public void PushEvent (HavEvent havEvent)
	{
		if (isShot && havEvent is InputHandler.MouseClickEvent)
			Explode ();
	}
}