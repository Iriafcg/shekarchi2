﻿using UnityEngine;
using System.Collections;

public class BombMultiple : BombNormal {

	private BombMultipleContainer container;
	public BombMultipleContainer Container
	{
		get
		{
			return container;
		}
		set
		{
			container = value;
		}
	}

	protected override void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Bomb")) return;
		var material = other.GetComponent<Masaleh>();
		if(material != null && material.materialName == "Glass"){
			OnExplosiveCollision (other.gameObject);
			return;
		}	
		
		var explosive = other.GetComponent<Explosive>();
		if (explosive != null)
		{
			explosive.Explode();
		}
		
		var enemy = other.gameObject.GetComponent<Enemy> ();
		if (null != enemy)
		{
			enemy.Destruction();
		}

		if (null == other.GetComponent<BombBase>())
		{
			OnExplosiveCollision (other.gameObject);

			Explode ();

		}
	}

	protected override void OnDestroy ()
	{
		container.bombExploded ();
		PlayExplosionEffects ();
	}
}
