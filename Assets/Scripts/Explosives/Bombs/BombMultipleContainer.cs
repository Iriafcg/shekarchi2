﻿using UnityEngine;
using System.Collections;
using Messaging;

public class BombMultipleContainer : MonoBehaviour, IShootable
{
	public float timeBetweenBombs;
	public float timeBetweenBursts;
	public GameObject bombPrefab;
	public int bombsPerBurst;
	public int numBursts;

	private BombBase[] bombs;

	private bool isActive = false;
	private float shootTime;
	private int bombsFired = 0;
	private int bombsExploded = 0;
	private GameObject cameraTarget;
	private Transform bombHatch;

	public Sprite image;
	public Sprite ButtonUI
	{
		get
		{
			return image;
		}
	}

	void Awake()
	{
		cameraTarget = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		cameraTarget.GetComponent<MeshRenderer> ().enabled = false;
		bombs = new BombBase [bombsPerBurst * numBursts];
		for (int i = 0; i < bombs.Length; ++i)
		{
			bombs [i] = GameObject.Instantiate (bombPrefab).GetComponent<BombBase>();
			bombs [i].transform.position = transform.position + Vector3.left * (float)i;
			bombs [i].transform.parent = transform;
			(bombs [i] as BombMultiple).Container = this;
		}
		bombHatch = GameObject.FindGameObjectWithTag ("BombSpawnPoint").transform;
	}

	public void ShootNext()
	{
		bombs [bombsFired].transform.parent = null;
		bombs [bombsFired].transform.position = bombHatch.position;
		bombs [bombsFired].enabled = true;
		bombs [bombsFired].GetComponent<Rigidbody2D>().gameObject.SetActive(true);
		bombs [bombsFired].Shoot ();
		++bombsFired;
	}

	public void Shoot ()
	{
		shootTime = Time.realtimeSinceStartup - timeBetweenBursts;
		GetComponent<SpriteRenderer> ().enabled = false;
		isActive = true;
	}

	public void bombExploded ()
	{
		++bombsExploded;
	}

	void Update()
	{
		if (isActive)
		{
			if (bombsExploded == bombs.Length)
			{
				bombsExploded = 0;
				Destroy(cameraTarget);
				isActive = false;
				MessageDispatcher.Instance.DispatchEvent(new BombBase.BombExplodedEvent());
				return;
			}

			float maxX = bombHatch.position.x, maxY = bombHatch.position.y;

			for (int i = 0; i < bombs.Length; ++i)
			{
				if (null != bombs[i] && bombs[i].isActiveAndEnabled)
				{
					if (bombs[i].transform.position.x > maxX) maxX = bombs[i].transform.position.x;
					if (bombs[i].transform.position.y > maxY) maxY = bombs[i].transform.position.y;
				}
			}
			cameraTarget.transform.position = new Vector3(maxX, maxY, 0);

			if(bombsFired < bombs.Length)
			{
				bool isBurstDone = bombsFired % bombsPerBurst == 0;
				float checkTime = isBurstDone ? timeBetweenBursts : timeBetweenBombs;
				if (Time.realtimeSinceStartup - shootTime >= checkTime)
				{
					ShootNext ();
					shootTime = Time.realtimeSinceStartup;
				}
			}
			else
			{
				//if (null != bombs[1]);
				//CameraFollow2D.Target = bombs[1].transform;
			}
		}
		else
		{
			if(null != cameraTarget)
				cameraTarget.transform.position = bombHatch.position;
		}
	}

	public Transform Position
	{
		get
		{
			return cameraTarget.transform;
		}
	}

	public void Equip(bool showTrajectory) {}
}
