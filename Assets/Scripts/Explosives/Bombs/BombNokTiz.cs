﻿using UnityEngine;
using System.Collections;

public class BombNokTiz : BombBase
{
	void OnTriggerEnter2D(Collider2D other)
    {
        if (null != other.gameObject.GetComponent<Attackable>()) // age az in masaleh bood, khodesh nabood beshe
        {
			other.GetComponent<Attackable>().Destruction();
        }
        else if (other.tag == "Ground")
        {
            Explode ();
        }
    }
}
