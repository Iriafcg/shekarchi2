using UnityEngine;
using System.Collections;

public class BombNormal : BombBase
{
    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("AntiMissle")) return;
        
        
        var material = other.GetComponent<Masaleh>();
        if(material != null && material.materialName == "Glass"){
            OnExplosiveCollision (other.gameObject);
            return;
        }	

        base.OnTriggerEnter2D(other);

        Explode ();   
    }
}