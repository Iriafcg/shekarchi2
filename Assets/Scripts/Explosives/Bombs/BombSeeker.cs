﻿using UnityEngine;
using System.Collections;
using Messaging;

public class BombSeeker : BombBase, ISubscriber
{
	private Camera cam;
	private bool isReleased = false;
	private bool isEquipped = false;
	private bool isSeeking = false;
	private float seekTime;
	private Vector3 seekTarget;
	private Vector3 endOfFlightMarker;
	private Transform bombHatch;

	public float seekDelay = .1f;

//	[SerializeField]
//	private float delay = .2f;
	[SerializeField]
	private float horizontalSpeed = 2;
	[SerializeField]
	private float forwardForce = 2.0f;
	[SerializeField]
	private float lateralForce = 15.0f;

	private Collider2D _collider2D;


	protected override void Awake()
	{
		base.Awake ();
		cam = Camera.main;
		myTrans = transform;
		endOfFlightMarker = GameObject.Find ("LevelEnd").transform.position;
		bombHatch = GameObject.Find ("BombHatch").transform;
		_collider2D = GetComponent<Collider2D>();
	}

	void Start()
	{
		SubscribeToEvent("SendJetIn");
	}

	public override void Equip (bool notUsable)
	{
		isEquipped = true;
	}

	public override void Shoot ()
	{
		base.Shoot ();
		seekTarget = cam.ScreenToWorldPoint(Input.mousePosition);
		isSeeking = true;
		seekTime = Time.realtimeSinceStartup;
		rigid.isKinematic = false; // temp
		_collider2D.enabled = true;
		
	}

	private void Seek()
	{
		var targetDiff = seekTarget - myTrans.position;
		//var targetDiff2 = new Vector2(targetDiff.x, targetDiff.y);
		//var outputDiff = targetDiff2 - rigid.velocity;

		rigid.AddForce(myTrans.right * forwardForce);

		var dot = Vector3.Dot (targetDiff.normalized, -myTrans.up);

		//float leftOrRight = Mathf.Sign(dot);

		rigid.AddForce (myTrans.up.normalized * Mathf.Sign(dot) * - Mathf.Sqrt(Mathf.Abs (dot)) * lateralForce);

		Debug.DrawRay (myTrans.position, myTrans.up.normalized * Mathf.Sign(dot) * - Mathf.Sqrt(Mathf.Abs (dot)) * lateralForce, Color.green);
		Debug.DrawRay (myTrans.position, myTrans.right.normalized * forwardForce, Color.red);
		Debug.DrawRay (myTrans.position, targetDiff, Color.blue);
	}

	protected override void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("AntiMissle")) return;

		base.OnTriggerEnter2D(other);
		
		var material = other.GetComponent<Masaleh>();
		if(material != null && material.materialName == "Glass"){
			OnExplosiveCollision (other.gameObject);
			return;
		}	
		
		if (null == other.GetComponent<BombBase>())
		{
			OnExplosiveCollision (other.gameObject);
		}
		Explode ();
	}

	protected override void FixedUpdate ()
	{
		base.FixedUpdate ();

		if (isReleased)
		{
			if (isSeeking && Time.realtimeSinceStartup - seekTime >= seekDelay)
			{
				Seek ();
			}
			else
			{
				if (myTrans.position.x > endOfFlightMarker.x)
				{
					Explode();
				}
			}
		}
	}

	protected override void OnDestroy ()
	{
		Unsubscribe("SendJetIn");
		base.OnDestroy ();
	}

	#region ISubscriber implementation

	public void SubscribeToEvent (string eventName)
	{
		MessageDispatcher.Instance.Subscribe(this, eventName);
	}

	public void Unsubscribe (string eventName)
	{
		MessageDispatcher.Instance.Unsubscribe(this, eventName);
	}

	public void PushEvent (HavEvent havEvent)
	{
		if (isEquipped && havEvent is HavCamera.SendJetInEvent)
		{
			isReleased = true;
			// rigid.position = bombHatch.position;
			// rigid.velocity = new Vector2(horizontalSpeed, 0);
		}
	}

	#endregion
}
