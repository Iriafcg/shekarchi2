using UnityEngine;
using System.Collections;

public class BombToopi : BombBase
{
    public int delay = 3;
	public int instantDestructions = 2;

	float explosionTime;
	bool isShot = false;

    public override void Shoot()
    {
		base.Shoot ();
		isShot = true;
        explosionTime = Time.time + delay;
    }

	protected override void OnCollisionEnter2D(Collision2D other)
    {
		base.OnCollisionEnter2D(other);

		var otherAttackable = other.gameObject.GetComponent<Attackable> ();
		if (instantDestructions > 0 && null != otherAttackable)
		{
			if (otherAttackable is Masaleh && (((Masaleh)otherAttackable).materialName == "Wood" || ((Masaleh)otherAttackable).materialName == "Mud" || ((Masaleh)otherAttackable).materialName == "Glass"))
			{
				otherAttackable.Destruction ();
				--instantDestructions;
			}
		}
    }
	
	protected override void OnTriggerEnter2D(Collider2D other)
	{
		var material = other.GetComponent<Masaleh>();
		if(material != null && material.materialName == "Glass"){
			OnExplosiveCollision (other.gameObject);
		}	

	}

    protected override void FixedUpdate()
    {
		if (isShot && Time.time > explosionTime)
        {
            Explode ();
        }
    }
}
