﻿using UnityEngine;
using System.Collections;

public class BulletRagbar : BombMultiple
{
	public float verticalSpeed = 7f;
	
	protected override void OnTriggerEnter2D(Collider2D other)
	{
        
        
		var material = other.GetComponent<Masaleh>();
		if(material != null && material.materialName == "Glass"){
			OnExplosiveCollision (other.gameObject);
			return;
		}	

		base.OnTriggerEnter2D(other);

		Explode ();   
	}
	public override void Shoot ()
	{
		base.Shoot ();
		rigid.velocity +=  Vector2.down * shootVelocity;
		rigid.isKinematic = false;
	}
}