﻿using UnityEngine;
using System.Collections;

public class BulletSnipe : BombMultiple
{
	public float verticalSpeed = 7f;
	
	protected override void OnTriggerEnter2D(Collider2D other)
	{
		var enemy = other.gameObject.GetComponent<Enemy> ();
		if (null != enemy)
		{
			enemy.Destruction();
		}

		if (null == other.GetComponent<BombBase>())
		{
			OnExplosiveCollision (other.gameObject);
			StartCoroutine(SelfDestruction());

		}

	}

	public IEnumerator SelfDestruction()
	{
		yield return new WaitForSeconds(0.8f);
		Explode();
	}

	public virtual void Explode(bool destroy = true, string soundName = "Explosion")
	{
		base.Explode();
		trail.transform.parent = null;
	}

	
	public override void Shoot ()
	{
		AudioManager.PlaySound("Snipe");
		base.Shoot ();
		rigid.velocity +=  Vector2.down * shootVelocity;
		rigid.isKinematic = false;
	}
}