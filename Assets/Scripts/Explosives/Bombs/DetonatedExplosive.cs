﻿using UnityEngine;
using System.Collections;

public class DetonatedExplosive : Explosive
{
	public void Detonate()
	{
		Explode ();
	}
}
