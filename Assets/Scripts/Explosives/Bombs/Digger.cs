﻿using System;
using UnityEngine;
using System.Collections;
using Messaging;

public class Digger : BombNormal
{
    private GameObject trail;
    public GameObject Toopi;
    public ParticleSystem[] particles;
    public AudioSource drillAudioSource;

    public bool isHit;
    protected override void FixedUpdate()
    {
        if (!isHit)
        {
            Vector3 forward = rigid.velocity;
            var up = Vector3.Cross (forward, Vector3.back);
            rigid.rotation = -Quaternion.LookRotation(Vector3.back, up).eulerAngles.z; 
        }

    }

    protected override void Awake()
    {
        myTrans = transform;
        rigid = gameObject.GetComponent<Rigidbody2D>();
        explosionWave = GameObject.Instantiate(explosionEffectPrefab).GetComponent<ParticleSystem>();
        explosionWave.transform.position = transform.position;
        explosionWave.transform.parent = transform.parent;

        foreach (var particle in particles)
        {
            particle.gameObject.SetActive(false);
        }
    }

    public float speed;
    private GameObject target;
    
    
    protected override void OnCollisionEnter2D(Collision2D collision)
    {

        
        if (!collision.collider.CompareTag("Soil"))
        {
            

        base.OnCollisionEnter2D(collision);
        isHit = true;
        StartCoroutine(DelayExplode());
        }



    }

    public IEnumerator DelayExplode()
    {
        yield return new WaitForSeconds(1);
        MessageDispatcher.Instance.DispatchEvent(new BombExplodedEvent());
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        target = other.gameObject;

        if (other.CompareTag("Shield"))
        {
            Explode();
        }
        
        if (other.CompareTag("Soil"))
        {

        foreach (var particle in particles)
        {
            particle.gameObject.SetActive(true);
        }

        trail = GameObject.Instantiate(trailPrefab);
        trail.transform.position = this.transform.position;
        trail.transform.parent = this.transform;
        rigid.velocity = rigid.velocity * speed;
        rigid.gravityScale = speed;
        drillAudioSource.Play();
        }
        else
        {
            GetComponent<BoxCollider2D>().enabled = true;
            GetComponent<CircleCollider2D>().enabled = false;
        }
        // else
        // {
        //     // Explode();
        // }


        // if (other.CompareTag("AntiMissle") || other.CompareTag("Enemy")) return;
        //       
        //       
        // var material = other.GetComponent<Masaleh>();
        // if(material != null && material.materialName == "Glass"){
        // 	OnExplosiveCollision (other.gameObject);
        // 	return;
        // }
        //
        // // base.OnTriggerEnter2D(other);
        // //
    }


    private void OnTriggerExit2D(Collider2D other)
    {

            drillAudioSource.Stop();
            trail.transform.parent = null;
            foreach (var particle in particles)
            {
                particle.transform.parent = null;
                particle.Stop();
            }

            Explode();
    }


    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (target.CompareTag("Soil"))
        {
            var bomb = Instantiate(Toopi, transform.position, Quaternion.identity);
            bomb.GetComponent<BombToopi>().Shoot();  
        }
    }
}