﻿using UnityEngine;
using System.Collections;

public class Detonator : MonoBehaviour
{
	public Sprite triggeredSprite;
	private SpriteRenderer spriteRenderer;
	bool isTriggered = false;

	[SerializeField]
	DetonatedExplosive[] explosives = null;


	void Awake()
	{
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (isTriggered)
			return;

		isTriggered = true;
		if ( null != triggeredSprite )
			spriteRenderer.sprite = triggeredSprite;
		for (int i = 0; i < explosives.Length; ++i)
		{
			explosives[i].Detonate ();
		}
	}
}
