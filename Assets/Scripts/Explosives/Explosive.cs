﻿using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour
{
	public LayerMask destroyable;
	public float explosionRadius = 5;
	public float explosionForce = 3000;
	public int hitDamage = 5;

	[SerializeField]
	protected GameObject explosionEffectPrefab;

	protected bool hasExploded = false;
	protected Transform myTrans;
	protected Rigidbody2D rigid;
	protected ParticleSystem explosionWave;
	protected Collider2D hit = null;

	protected virtual void Awake()
	{
		myTrans = transform;
		rigid = gameObject.GetComponent<Rigidbody2D>();
		explosionWave = GameObject.Instantiate(explosionEffectPrefab).GetComponent<ParticleSystem>();
		explosionWave.transform.position = transform.position;
		explosionWave.transform.parent = transform.parent;
	}

	public virtual void Explode(bool destroy = true,string soundName = "Explosion")
	{
		if (hasExploded)
			return;

		hasExploded = true;

		Collider2D[] colliders = Physics2D.OverlapCircleAll(myTrans.position, explosionRadius, destroyable);
		
		for (int i = 0; i < colliders.Length; ++i)
		{
			hit = colliders[i];
			print(hit.name);
			if (hit.gameObject == this.gameObject)	// TODO: Too intrusive. Maybe think of a better way?
				continue;

			Vector2 dir = (hit.transform.position - myTrans.position);
			//float wearoff = 1 - (dir.magnitude / explosionRadius); // damage attenuation factor
			//
			//			if (wearoff > wearoffThreshold) // age kheyli nazdik bood be noghte enfejar, nabood beshe
			//				Destroy(hit.gameObject);
			//			else
			//			{
			//				if (null != hit.gameObject.GetComponent<Attackable>())
			//				{
			//					hit.GetComponent<Attackable>().ReceiveDamage(hitDamage);
			//					hit.GetComponent<Rigidbody2D>().AddForce(dir.normalized * explosionForce * wearoff); // emale niroo
			//
			//					if (null != hit.gameObject.GetComponent<Masaleh>())
			//					{
			//						hit.GetComponent<Rigidbody2D>().AddTorque(explosionForce * wearoff / 5); // emale nirooye charkheshi baraye tabi'itar shodane kar
			//					}
			//				}
			//			}

			float attenuationFactor = Mathf.Exp(-dir.magnitude / explosionRadius);

//			var damageAmount = (dir.magnitude < explosionRadius * .5f) ? hitDamage : .5f * hitDamage;
//			var force = (dir.magnitude < explosionRadius * .5f) ? explosionForce : .5f * explosionForce;

			var damageAmount = hitDamage * attenuationFactor;
			var force = explosionForce * attenuationFactor;

			if (null != hit.GetComponent<IDamageable>())
			{
				hit.GetComponent<IDamageable>().ReceiveDamage(damageAmount);
			}
			hit.GetComponent<Rigidbody2D>().AddForce(dir.normalized * force);
			hit.GetComponent<Rigidbody2D>().AddTorque(force * .2f);
		}

		if (PlayerPrefs.GetInt ("VibrateOption", 0) == 1)
		#if UNITY_ANDROID
			Handheld.Vibrate ();
		#endif

		AudioManager.PlaySound (soundName);
		
		if (destroy)
			Destroy(gameObject);
	}
	

	protected void OnExplosiveCollision (GameObject other)
	{
		if (other.tag == "Ground")
			return;
		
		var otherAsDamageable = other.GetComponent<IDamageable> ();

		if (null != otherAsDamageable)
		{
			if (otherAsDamageable.Resistance <= hitDamage)
				otherAsDamageable.Destruction ();
		}
	}

	protected void PlayExplosionEffects()
	{
		if (null != explosionWave)
		{
			explosionWave.transform.position = myTrans.position;
			explosionWave.Play();
			Destroy(explosionWave.gameObject, explosionWave.duration);
		}
		else
		{
			Debug.LogWarning("Explosion Wave is null.");
		}
	}

	protected virtual void OnDestroy()
	{
		PlayExplosionEffects ();
		ExplodeNeighbours();
	}

	public void ExplodeNeighbours()
	{
		Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius, destroyable);
		for (int i = 0; i < colliders.Length; ++i)
		{
			var hit = colliders[i].gameObject;
			if (hit.gameObject == this.gameObject)
				continue;
			if (hit.GetComponent<Explosive>() != null && hit.GetComponent<Flammable>() == null)
			{
				hit.GetComponent<Explosive>().Explode();
			}
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
#if UNITY_EDITOR
		for (int i = 0; i < 360; i += 18) // 20 sides should be more than enough
		{			
			Gizmos.DrawLine(transform.position + explosionRadius * new Vector3(Mathf.Cos(i * Mathf.PI / 180), Mathf.Sin(i * Mathf.PI / 180), -5.0f),
				transform.position + explosionRadius * new Vector3( Mathf.Cos((i+18) * Mathf.PI / 180), Mathf.Sin((i+18) * Mathf.PI / 180), -5.0f));
		}
#endif
	}
}
