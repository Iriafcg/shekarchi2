﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Flammable : Explosive, IDamageable
{
	[SerializeField]
	int resistance = 20;
	public float speedThreshold;
	public float timeBeforeExplosion = 3;
	public float fireDamageThreshold = 5;
	public int score = 500;

	protected float explosionTime;
	protected bool isBurning = false;
	public ParticleSystem flameParticle;

	public int Resistance
	{
		get
		{
			return resistance;
		}
	}

	public int Score
	{
		get
		{
			return score;
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		AudioManager.PlaySound ("OilDrum");
		if (collision.relativeVelocity.magnitude  > speedThreshold)
		{
			StartFlame ();
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (null != other.gameObject.GetComponent<BombBase>())
			Explode ();
	}

	protected virtual void FixedUpdate()
	{
		if (isBurning && Time.time > explosionTime)
		{
			isBurning = false;
			Explode ();
		}
	}

	public void StartFlame()
	{
		if (!isBurning)
		{

			if(flameParticle != null) flameParticle.Play ();
			isBurning = true;
			explosionTime = Time.time + timeBeforeExplosion;
		}
	}

	protected override void OnDestroy()
	{
		base.OnDestroy ();
		Destroy (flameParticle, flameParticle.main.duration);
		Destruction ();
	}

	public void ReceiveDamage (float amount)
	{
		if (amount > fireDamageThreshold)
		{
			StartFlame();
		}
	}

	public void Destruction ()
	{
		ScoreManager.Score += Score;
	}
}
