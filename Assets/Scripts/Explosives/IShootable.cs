﻿using UnityEngine;
using System.Collections;

public interface IShootable
{
	void Shoot();
	void Equip(bool showTrajectory);
	Transform Position { get; }
	Sprite ButtonUI {get;}
}
