using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Vehicle : Flammable, IDamageable
{
	public Animator animator;
	ParticleSystem smoke = null;
	public UnityEvent onExplode;


	protected override void Awake()
	{
		base.Awake ();
		smoke = gameObject.GetComponentInChildren<ParticleSystem> ();

		if (GetComponent<Animator>() != null)
		{
			animator = GetComponent <Animator>();
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		var bomb = collider.gameObject.GetComponent<BombBase> ();
		if (null != bomb)
		{
			StartFlame ();

			// Explode(false);
			// smoke.Stop();
			// PlayExplosionEffects();
		}
	}

	protected override void OnDestroy()
	{
		onExplode.Invoke();

		// Explode(false);
		// smoke.Stop();
		PlayExplosionEffects();
		animator.SetBool("destroyed", true);
		// base.OnDestroy();

		// onExplode.Invoke();
	}

	// public void OnDestroy()
	// {
	// 	onExplode.Invoke();
	// }

	#region IDamageable implementation

	protected override void FixedUpdate()
	{
		if (isBurning && Time.time > explosionTime && !hasExploded)
		{
			isBurning = false;
			Explode (false);
			animator.SetBool("destroyed", true);
			onExplode.Invoke();
			PlayExplosionEffects();
			Destroy(flameParticle);
			smoke.Stop();
			hasExploded = true;

		}
	}

	public int Resistance
	{
		get
		{
			return 1;
		}
	}

	public int Score
	{
		get
		{
			return 1;
		}
	}

	#endregion
}
