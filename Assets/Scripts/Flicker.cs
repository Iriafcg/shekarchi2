﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D;

public class Flicker : MonoBehaviour
{
    private bool active = true;
    public GameObject area;
    private float time;
    public float flickingTime;
    private SpriteShapeRenderer _spriteShapeRenderer;
    private Collider2D _collider;

    private void Awake()
    {
        _collider = area.GetComponent<Collider2D>();
        _spriteShapeRenderer = area.GetComponent<SpriteShapeRenderer>();
    }

    public void Update()
    {
if(flickingTime == 0) return;
        time += Time.deltaTime;

        if (time > flickingTime)
        {
            active = !active;
            _spriteShapeRenderer.enabled = active;
            _collider.enabled = active;
            time = 0;
        }
    }

}
