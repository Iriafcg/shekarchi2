﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public Transform target;

    void Update()
    {
        target.rotation = transform.rotation;
        target.position = new Vector3(transform.position.x,transform.position.y,target.position.z);
    }
}
