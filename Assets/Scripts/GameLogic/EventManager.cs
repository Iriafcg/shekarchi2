﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Animation))] 
public class EventManager : MonoBehaviour 
{
    Animation anim;
    AnimationManager pauseAnim;
    //bool finish = false; // baraye levelcompelete va gameover

    public static EventManager instance;
    
    void Awake()
    {
        instance = this;
        pauseAnim = GetComponent<AnimationManager>();
        instance.anim = GetComponent<Animation>();
    }

    #region Events
    void OnEnable()
    {
        //Motorcycle_Controller.OnGameover += Gameover;
        //BodyTrigger.OnLevelCompelete += LevelCompelete;
//        InGameButtons.OnUnPause += unPaused;
//        InGameButtons.OnPause += Paused;
    }

    void OnDisable()
    {
        //Motorcycle_Controller.OnGameover -= Gameover;
        //BodyTrigger.OnLevelCompelete -= LevelCompelete;
//        InGameButtons.OnUnPause -= unPaused;
//        InGameButtons.OnPause -= Paused;
    }
    #endregion

    public void Paused()
    {
		AudioManager.PlaySoundSingle ("Click");
		Fade.FadeIn ();
        anim["Pause"].speed = 1;
        anim["Pause"].time = 0;
        pauseAnim.PlayAnimation("Pause");
		AudioManager.PauseAll ();
        Time.timeScale = 0;
    }

    public void unPaused()
    {
		AudioManager.PlaySoundSingle ("Click");
		Fade.FadeOut ();
        anim["Pause"].speed = -1;
        anim["Pause"].time = anim["Pause"].length;
        anim.Play("Pause");
		AudioManager.UnpauseAll ();
        Time.timeScale = 1;
    }

    /*
    void Gameover()
    {
        if (!finish)
        {
            SaveMe.CalculateCost();
            saveMeHolder.gameObject.active = true;
            saveMeHolder.Play("SaveMe"); // idle e saveme ejra beshe
            instance.anim.Play("SaveMeEnter");
            instance.saveMeCount.enabled = true; // couter e 5 sanie shoro beshe
            finish = true;
        }
    }
    
    void Respawned()
    {
        finish = false;
    }

    void LevelCompelete()
    {
        finish = true;
        anim.Play("LevelCompelete");
    }
     * */
}
