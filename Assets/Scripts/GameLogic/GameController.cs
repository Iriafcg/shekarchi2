﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Automata;

public class GameController : StateMachine
{
	//bool isLevelComplete = false;

	private static GameController instance = null;

	public GameObject uiBombButtons;
	public Gameover gameOver;
	public LevelCompelete levelComplete;
	public SkipLevel skipLevel;
	public Bombs uiBombController;
	public EventManager pauseMenu;
	public InGameButtons inGameButtons;
	private Animator uiBombAnimator;
	public int extraBombBonusScore = 10000;
	public Text levelNameText = null;

	// TODO:	These can be move into a separate class -
	//			a level organizer or something that other classes can make use of as well

	Transform levelBegin, levelEnd, groundMarker;
	public Transform leftBoundary, rightBoundary, topBoundary, bottomBoundary;
	public float leftOffset, rightOffset, topOffset, bottomOffset;
	//public Vector4 leftOffset, rightOffset, topOffset, bottomOffset;

	bool setManualZoom = false;

	void OnLevelWasLoaded (int levelIndex)
	{
		if (levelIndex == PlayerPrefs.GetInt("LastLevel"))
		{
			if (-1 != PlayerPrefs.GetFloat("ManualZoom"))
			{
				setManualZoom = true;
			}
		}
	}

	protected override void Awake()
	{
		base.Awake ();

		levelBegin = GameObject.FindGameObjectWithTag("LevelBegin").transform;
		levelEnd = GameObject.FindGameObjectWithTag("LevelEnd").transform;
		groundMarker = GameObject.FindGameObjectWithTag("GroundMarker").transform;

		//levelNameText.text = "(" + SceneManager.GetActiveScene().buildIndex + ")" + SceneManager.GetActiveScene().path.Substring("Assets/Scenes/".Length);

		SetWorldBoundaries ();

		if (null == instance)
			instance = this;
	
		uiBombAnimator = uiBombButtons.GetComponent<Animator> ();

		Init start = new Init ();
		JetIn jetIn = new JetIn ();
		BombReleased bombReleased = new BombReleased ();
		CheckStatus checkStatus = new CheckStatus ();
		Paused paused = new Paused ();
		Win win = new Win ();
		Fail fail = new Fail ();
		WaitingOnJetToLeave waitingLeave = new WaitingOnJetToLeave ();
		WaitingOnJetToEnter waitingEnter = new WaitingOnJetToEnter ();

		AddState (start);
		AddState (jetIn);
		AddState (bombReleased);
		AddState (checkStatus);
		AddState (paused);
		AddState (win);
		AddState (fail);
		AddState (waitingEnter);
		AddState (waitingLeave);

		InitialState = start;

		AddTransition (start, waitingEnter, "BombEquipped");	// TODO: Event classes need static Name props and/or enums, string literals scream "Ooh I'm gonna forgot to change that!"
		AddTransition (waitingEnter, jetIn, "SendJetIn");
		AddTransition (jetIn, bombReleased, "MouseClick");
		AddTransition (jetIn, checkStatus, "JetOut");
		AddTransition (bombReleased, checkStatus, "BombExploded");
		AddTransition (checkStatus, waitingLeave, "NextTurn");
		//AddTransition (waiting, start, "JetOut");
		AddTransition (waitingLeave, start, "JetReady");
		AddTransition (checkStatus, checkStatus, "EnemyDied");
		AddTransition (checkStatus, win, "Win");
		AddTransition (checkStatus, fail, "Fail");
		AddTransition (checkStatus, checkStatus, "JetOut");
		AddTransition (checkStatus, waitingEnter, "BombEquipped");
		AddTransition (waitingLeave, waitingEnter, "BombEquipped");

		// TODO: an HFSM would solve these redundant transitions
		AddTransition (start, paused, "Pause");
		AddTransition (jetIn, paused, "Pause");
		AddTransition (bombReleased, paused, "Pause");
		AddTransition (checkStatus, paused, "Pause");
		AddTransition (waitingEnter, paused, "Pause");
		AddTransition (waitingLeave, paused, "Pause");

		AddTransition (start, checkStatus, "EnemyDied");
	}

	void SetWorldBoundaries ()
	{
		topBoundary.position = new Vector3 ((levelBegin.position + levelEnd.position).x * .5f, levelBegin.position.y + topOffset, 0);
		bottomBoundary.position = new Vector3 ((levelBegin.position + levelEnd.position).x * .5f, groundMarker.position.y - bottomOffset, 0);
		leftBoundary.position = new Vector3(levelBegin.position.x - leftOffset, (levelBegin.position + groundMarker.position).y * .5f,0);
		rightBoundary.position = new Vector3(levelEnd.position.x + rightOffset, (levelBegin.position + groundMarker.position).y * .5f,0);
	}

	protected override void Start()
	{
		base.Start ();

		SubscribeToEvent ("BombReleased");
		SubscribeToEvent ("BombEquipped");
		SubscribeToEvent ("BombExploded");
		SubscribeToEvent ("JetOut");
		SubscribeToEvent ("SendJetIn");
		SubscribeToEvent ("Pause");
		SubscribeToEvent ("Unpause");
		SubscribeToEvent ("Win");
		SubscribeToEvent ("Fail");
		SubscribeToEvent ("EnemyDied");
		SubscribeToEvent ("MouseClick");
		SubscribeToEvent ("JetReady");

		if (setManualZoom)
		{
			HavCamera.Instance.SetDefaultSize(PlayerPrefs.GetFloat("ManualZoom"));
		}
	}


	// TODO:	ideally, uiBombAnimator and uiBombController should be FSMs
	//			and act on events autonomously - Better yet, they can be sub-FSMs
	//			of GameController.

	public void ShowUI()
	{
		if (0 != Bombs.Instance.bombsLeft)
		{
			uiBombAnimator.SetInteger ("state", 0);
			uiBombController.EnableButtons ();
		}
	}

	public void HideUI()
	{
		uiBombAnimator.SetInteger ("state", 1);
	}

	public void DisableBombButtons()
	{
		uiBombController.DisableButtons ();
	}

	public static GameController Instance { get {return instance;} }

	public class Init : State
	{
		public override void OnEnter (HavEvent havEvent)
		{
			((GameController)stateMachine).ShowUI();
		}
	}

	public class JetIn : State
	{		
	}

	public class BombReleased : State
	{
		Transform target = null;
		public override void OnEnter (HavEvent havEvent)
		{
			if (null != (target = Bombs.Instance.ReleaseEquipped ()))
				DispatchEvent (new BombReleasedEvent(target));
		}
	}


	// TODO: These next two classes should probably be one, but I don't have time to be sure right now :D

	public class WaitingOnJetToEnter : State
	{
		public override void OnEnter (HavEvent havEvent)
		{
			((GameController)stateMachine).DisableBombButtons();
			((GameController)stateMachine).HideUI();
		}
	}

	public class WaitingOnJetToLeave : State
	{
		public override void OnEnter (HavEvent havEvent)
		{
			DispatchEvent(new QueryJetEvent());
		}
	}

	public class CheckStatus : State
	{
		bool keepChecking = true;
		int liveEnemyCount;
		int lastCheckedScore;
		float nextCheckTime = 0;
		int endCheckCount = 0;
		int maxEndCheckCount = 5;
		float checkInterval = 3;

		float failDelay = 0;
		float winDelay = 1.5f;

		GameObject[] enemies;
		Collider2D[] enemyColliders;

		public CheckStatus()
		{
			enemies = GameObject.FindGameObjectsWithTag("Enemy");
			enemyColliders = new Collider2D[enemies.Length];
			for (int i = 0; i < enemies.Length; i++)
				enemyColliders[i] = enemies[i].GetComponent<Collider2D> ();
		}

		public override void OnEnter (HavEvent havEvent)
		{
			keepChecking = true;
			endCheckCount = 0;
			nextCheckTime = 0;

			if (havEvent is Jet.JetOutEvent)	// TODO: ENCAPSULATION! DECOUPLING!
			{
				((GameController)stateMachine).ShowUI ();
				return;
			}

//			CameraInternalStateMonitor.Instance.AddEntry("ENTERING ON " + havEvent.Name);

			if (havEvent is BombBase.BombExplodedEvent)
			{
				failDelay = 3.0f;
			}
		}

		public override void Update ()
		{
			if (endCheckCount == 0 || keepChecking && endCheckCount < maxEndCheckCount && Time.time > nextCheckTime)
			{
				Debug.Log("***CHECKING***" + Time.time);
	//			CameraInternalStateMonitor.Instance.AddEntry("CHECKING @ " + Time.time);
				if (endCheckCount == 0 || lastCheckedScore != ScoreManager.Score)
				{
					if (0 == CountLiveEnemies ())
					{
						Debug.Log("***WINNING***" + Time.time);
		//				CameraInternalStateMonitor.Instance.AddEntry("WINNING @ " + Time.time);
						DispatchEvent (new WinEvent(), winDelay);
						endCheckCount++;
						keepChecking = false;
					}
					else
					{
						nextCheckTime = Time.time + checkInterval;
						Debug.Log("***REPEATING @ " + nextCheckTime + "***" + Time.time);
	//					CameraInternalStateMonitor.Instance.AddEntry("REPEATING @ " + nextCheckTime + " @ " + Time.time);
						lastCheckedScore = ScoreManager.Score;
						endCheckCount++;
					}
				} 
				else
				{
					if (Bombs.BombsLeft == 0)
					{
						Debug.Log("***FAILING***" + Time.time);
	//					CameraInternalStateMonitor.Instance.AddEntry("FAILING @ " + Time.time + ", DELAY " + failDelay);
						DispatchEvent (new FailEvent(), failDelay);
					}
					else
					{
						Debug.Log("***AGAINING***" + Time.time);
	//					CameraInternalStateMonitor.Instance.AddEntry("AGAINING @" + Time.time);
						PushAndDispatchEvent (new NextTurnEvent());
					}
					keepChecking = false;
				}
			}
			else
			{
				//	Idly wait for scheduled event to fire
			}
		}

		int CountLiveEnemies ()
		{
			liveEnemyCount = 0;
			for (int i = 0; i < enemies.Length; i++)
			{
				if (enemyColliders [i] != null && enemyColliders [i].enabled == true)
				{
					liveEnemyCount++;
				}
			}
			return liveEnemyCount;
		}
	}

	public class Paused : State
	{
		private Transition unpauseTransition;
		//private Transition mouseClickTransition;

		public override void OnEnter (HavEvent havEvent)
		{
			((GameController)stateMachine).DisableBombButtons();
			((GameController)stateMachine).HideUI();
			unpauseTransition = stateMachine.AddTransition (this, stateMachine.previousState, "Unpause"); // TODO: SMELLS LIKE HELL for more than one reason (the line in general and the "previousState" concept)
			//mouseClickTransition = stateMachine.AddTransition (this, stateMachine.previousState, "MouseClick"); // TODO: SMELLS LIKE HELL for more than one reason (the line in general and the "previousState" concept)
			Time.timeScale = 0;
//			((GameController)stateMachine).pauseMenu.Paused ();
			Debug.Log("ENTERING PAUSED");
		}

		public override void OnLeave (HavEvent havEvent)
		{
			stateMachine.RemoveTransition (unpauseTransition); // TODO: SMELLS LIKE HELL
			//stateMachine.RemoveTransition (mouseClickTransition); // TODO: SMELLS LIKE HELL
//			((GameController)stateMachine).pauseMenu.unPaused ();
			InGameButtons.instance.DoUnPause ();
			Time.timeScale = 1;
			Debug.Log("LEAVING PAUSED");
		}
	}
	
	

	public class Win : State
	{
		public override void OnEnter (HavEvent havEvent)
		{
			
			((GameController)stateMachine).inGameButtons.DisableButtons ();
			((GameController)stateMachine).DisableBombButtons();
			((GameController)stateMachine).HideUI();
//			if (Bombs.BombsLeft > 0)

//				ScoreManager.Score += Bombs.BombsLeft * ((GameController)stateMachine).extraBombBonusScore;
			((GameController)stateMachine).levelComplete.CompleteLevel(Bombs.BombsLeft);
		}
	}

	public class Fail : State
	{
		public override void OnEnter (HavEvent havEvent)
		{
			((GameController)stateMachine).DisableBombButtons();
			((GameController)stateMachine).HideUI();
			((GameController)stateMachine).inGameButtons.DisableButtons ();
			((GameController)stateMachine).gameOver.game_over();
		}
	}

	#region Events
	public class BombReleasedEvent : HavEvent
	{
		public Transform Target {get; set;}

		public BombReleasedEvent(Transform target)
		{
			Target = target;			
		}

		public override string Name
		{
			get
			{
				return "BombReleased";
			}
		}
	}

	public class WinEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "Win";
			}
		}
	}

	public class FailEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "Fail";
			}
		}
	}

	public class NextTurnEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "NextTurn";
			}
		}
	}

	public class QueryJetEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "QueryJet";
			}
		}
	}

	#endregion

}
