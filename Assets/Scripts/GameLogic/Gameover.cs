﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Gameover : MonoBehaviour {

	public string[] titles;

    Animation anim;
	Text txtTitle;

    void Awake()
    {
        anim = GetComponent<Animation>();
		txtTitle = transform.GetChild (0).GetComponent<Text> ();
    }

    public void game_over()
    {
		txtTitle.text = titles [Random.Range (0, titles.Length)];
        anim.Play();
		Fade.FadeIn ();
		AudioManager.PlaySoundSingle ("GameOver");
    }
}
