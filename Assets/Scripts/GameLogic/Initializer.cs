﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class Initializer : MonoBehaviour
{
    void Start()
    {
        // Enable line below to enable logging if you are having issues setting up OneSignal. (logLevel, visualLogLevel)
        //OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);

        print("Initializing...");
        Application.targetFrameRate = 60;
        PlayerPrefs.SetInt("LastLevel", SceneManager.GetActiveScene().buildIndex);


        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); 
    }

    // Gets called when the player opens the notification.
    private static void HandleNotification(string message, Dictionary<string, object> additionalData, bool isActive)
    {
    }
}
