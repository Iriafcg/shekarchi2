﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Automata;

public class Jet : StateMachine
{
	public AudioClip[] engineClipSounds;
	public int speed = 7;

	Transform levelEndMarker;
	Transform levelBeginMarker;

	private Rigidbody2D rigid;
	private Vector3 jetStartPos;
	private AudioSource engineSound;

	public float moveEndOffsetX = 2;
	public float extraOffsetBecauseTrajectory = 0.0f;
	public Vector2 jetSpawnOffset = new Vector2 (4.0f, 1.7f);

	private static Jet instance;

	public Sprite[] sprites;
	public SpriteRenderer jetSpriteRenderer;
	public GameObject jetEffect;

	protected override void Awake()
	{
		base.Awake();

		levelBeginMarker = GameObject.FindGameObjectWithTag("LevelBegin").transform;
		levelEndMarker = GameObject.FindGameObjectWithTag("LevelEnd").transform;

		instance = this;
		jetStartPos = levelBeginMarker.position - new Vector3(jetSpawnOffset.x, jetSpawnOffset.y, 0.0f);
		rigid = GetComponent<Rigidbody2D> ();
		rigid.position = jetStartPos;
		engineSound = GetComponent<AudioSource>();
		if (PlayerPrefs.GetInt ("AudioOption", 1) == 0)
			engineSound.mute = true;


		Initial init = new Initial();
		Moving moving = new Moving();
		Waiting waiting = new Waiting();
		Ready ready = new Ready();

		InitialState = init;
		AddState(init);
		AddState(moving);
		AddState(waiting);
		AddState(ready);

		AddTransition(init, moving, "SendJetIn");
		AddTransition(moving, init, "JetOut");
		AddTransition(moving, waiting, "NextTurn");
		AddTransition(waiting, init, "JetReady");

		AddTransition(init, ready, "NextTurn");
		AddTransition(ready, init, "JetReady");
		AddTransition(ready, init, "JetOut");
		AddTransition(ready, moving, "SendJetIn");
	}

	protected override void Start()
	{
		base.Start();
		SubscribeToEvent("SendJetIn");
		SubscribeToEvent("NextTurn");
		SubscribeToEvent("JetOut");
		SubscribeToEvent("JetReady");
		SubscribeToEvent("QueryJet");
		int index = PlayerPrefs.GetInt("AirPlane", 0);
		if (index == 5)
		{
			jetEffect.SetActive(true);
		}
		jetSpriteRenderer.sprite = sprites[index];
		print(index);

	}

	#region States

	class Initial : State
	{
		public override void OnEnter (HavEvent havEvent)
		{
			foreach (var sprite in instance.GetComponentsInChildren<SpriteRenderer> ())
			{
				sprite.enabled = false;
			}
			
			instance.rigid.position = instance.jetStartPos - ((Jet)stateMachine).extraOffsetBecauseTrajectory * Vector3.right;
			instance.engineSound.Stop ();
			instance.rigid.velocity = Vector3.zero;
		}
	}

	class Moving : State
	{
		public override void OnEnter (HavEvent havEvent)
		{
			foreach (var sprite in instance.GetComponentsInChildren<SpriteRenderer> ())
			{
				sprite.enabled = true;
			}
			instance.GetComponentInChildren<TrailRenderer>().enabled = true;
			instance.GetComponentInChildren<ParticleSystem>(true).gameObject.SetActive(true);
			instance.rigid.velocity = instance.speed * Vector3.right;

			instance.engineSound.clip = instance.engineClipSounds [Random.Range (0, instance.engineClipSounds.Length)];
			instance.engineSound.Play ();
		}
		public override void Update ()
		{
			if (instance.rigid.position.x > instance.levelEndMarker.position.x + instance.moveEndOffsetX)
			{
				DispatchEvent (new JetOutEvent());
			}
		}

		public override void OnLeave (HavEvent havEvent)
		{
			instance.GetComponentInChildren<TrailRenderer>().enabled = false;
			instance.GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
		}
	}

	class Waiting : State
	{
		public override void Update ()
		{
			if (instance.rigid.position.x > instance.levelEndMarker.position.x + instance.moveEndOffsetX)
			{
				DispatchEvent (new JetReadyEvent());
			}
		}
	}

	class Ready : State
	{
		public override void OnEnter (HavEvent havEvent)
		{	
			DispatchEvent(new JetReadyEvent(), .5f);
		}
	}

	#endregion

	#region Events

	public class JetOutEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "JetOut";
			}
		}
	}

	public class JetReadyEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "JetReady";
			}
		}
	}

	#endregion
}
