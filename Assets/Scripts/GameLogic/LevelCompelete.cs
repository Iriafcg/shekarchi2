﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelCompelete : MonoBehaviour {
    
	public GameObject starFireWork;
	public float interval = 1;
	public string[] ThreeStarTitles;
	public string[] TwoStarTitles;
	public string[] OneStarTitles;

	private Vector3 starFireworkBasePos = new Vector3 (-195, -200, 0); // sorry dude :D
	private string[][] titles;

	new Animation animation;

    Text txtScore, txtRecord, txtTitle, txtRemainingBombsScore;
	ParticleSystem firstStars, secondStars, thirdStars, lightGlow;
	float timeToCountScore, startCountingTime;
	AudioSource starCountingAud;

	bool compeleted = false;
	float nextFireWork;

	string starCountPath;
	string levelRecordPath;

	int currentRecord = -1;
	int currentStarCount = -1;

	bool newHighScore = false;

	public Bombs bombs;

	public bool IsTutorialLevel;

    void Awake()
    {
		//PlayerPrefs.DeleteAll ();
		bombs = FindObjectOfType<Bombs>();
		animation = GetComponent<Animation>();
		starCountingAud = GetComponent<AudioSource> ();
        txtScore = transform.GetChild(0).GetComponent<Text>();
		txtRecord = transform.GetChild(1).GetComponent<Text>();
		txtTitle = transform.GetChild(2).GetComponent<Text>();
		firstStars = transform.GetChild (3).GetComponent<ParticleSystem> ();
		secondStars = transform.GetChild (4).GetComponent<ParticleSystem> ();
		thirdStars = transform.GetChild (5).GetComponent<ParticleSystem> ();
		lightGlow = transform.GetChild (transform.childCount - 2).GetComponent<ParticleSystem> ();
		txtRemainingBombsScore = transform.GetChild (transform.childCount - 1).GetComponent<Text> ();

		starCountPath = (SceneManager.GetActiveScene ().buildIndex - 1) + "_Stars";
		levelRecordPath = (SceneManager.GetActiveScene ().buildIndex - 1) + "_Record";

		currentRecord = -1;
		currentStarCount = -1;
    }

	void Start()
	{
		#region Ugly conversion of public inspector values to neat arrays.
		// Hopefully we'll find a better way to do this some time. Or not.

		titles = new string[3][];
		titles[0] = OneStarTitles;
		titles[1] = TwoStarTitles;
		titles[2] = ThreeStarTitles;

		#endregion
	}

	int remainingBombsBonus;
	public void CompleteLevel (int remainingBombs)
    {
		if (remainingBombs > 0) 
		{
			remainingBombsBonus = remainingBombs * 10000;
			txtRemainingBombsScore.text = "+ " + remainingBombsBonus.ToString ();

			animation.Play ("RemainingBombScore");
			StartCoroutine (Delay (2.5f));
			AudioManager.PlaySoundSingle ("ShowUp");
		} 
		else 
		{
			StartCoroutine (Delay (1.5f));
		}
    }

	public void AddRemainingBonus()
	{
		ScoreManager.Score += remainingBombsBonus;
		AudioManager.PlaySoundSingle ("HighScoreBonus");
	}

    IEnumerator Delay (float delay)
    {
        yield return new WaitForSeconds (delay);
		compeleted = true;
        //txtScore.text = ScoreManager.Score.ToString();
		txtScore.text = "0";
		startCountingTime = Time.time;
		starCountingAud.Play ();

		Fade.FadeIn ();
		AudioManager.PlaySoundSingle ("Swip");

		// TODO: Should this be uncommented? My personal preference is NO WAY! -alb
        if (PlayerPrefs.GetInt("currentLevel", 1) < SceneManager.GetActiveScene().buildIndex + 1)
		PlayerPrefs.SetInt ("currentLevel", SceneManager.GetActiveScene().buildIndex);

		animation.Play ();

		currentStarCount = GetStarCount();
		SetCountVariables ();
		currentRecord = CheckAndUpdateLevelRecord ();
    }

	int GetStarCount ()
	{
		if (IsTutorialLevel) return 3;
		
		var stars = bombs.bombsLeft + 1;
		return stars > 0 ? stars : -1; // This should never happen!
	}

	void SetCountVariables ()
	{
		int previousStars = PlayerPrefs.GetInt(starCountPath, 0);
		int starCount = GetStarCount ();
		if (starCount < 0)
		{
			Debug.LogError ("Something is horribly wrong!");
		}

		txtTitle.text = titles[starCount - 1][Random.Range(0 , titles[starCount - 1].Length)];
		timeToCountScore = (float)starCount;
		animation.PlayQueued (starCount + "Star");

		if (previousStars < starCount)
		{
			PlayerPrefs.SetInt(starCountPath, starCount);
		}
	}

	int CheckAndUpdateLevelRecord ()
	{
		int record = PlayerPrefs.GetInt(levelRecordPath, 0);
		txtRecord.text = record.ToString () + " :ﺩﺭﻮﻛﺭ ﻦﻳﺮﺘﻬﺑ";

		if (record < ScoreManager.Score)
		{
			PlayerPrefs.SetInt(levelRecordPath, ScoreManager.Score);
			txtRecord.text =ScoreManager.Score.ToString() + " :ﺩﺭﻮﻛﺭ ﻦﻳﺮﺘﻬﺑ";
			if (!newHighScore)
			{
				animation.PlayQueued ("NewHighScore");
				newHighScore = true;
			}
		}

		return record;
	}

	void Update()
	{
		if (compeleted) 
		{
			if (nextFireWork < Time.time) 
			{
				nextFireWork = Time.time + interval;
				// TODO: GET RID OF THE MAGIC NUMBERS!
				GameObject fireWork = (GameObject)GameObject.Instantiate (starFireWork, new Vector3 (Random.Range (-6, 6) + starFireworkBasePos.x, Random.Range (-4, 4) + starFireworkBasePos.y, -1), Quaternion.identity);
				AudioManager.PlaySound("FireWork");
				Destroy(fireWork.gameObject, fireWork.GetComponent<ParticleSystem>().duration);
			}
			var rollingScore = Mathf.Lerp (0, ScoreManager.Score, Mathf.InverseLerp (0, timeToCountScore, Time.time - startCountingTime));

			if (GetStarCount() > currentStarCount)
			{
				currentStarCount = GetStarCount();
				SetCountVariables ();
			}

			if (rollingScore > currentRecord)
			{
				currentRecord = CheckAndUpdateLevelRecord();
			}

			txtScore.text = rollingScore.ToString ("0");
		}
	}

	public void ThirdStarsParticle()
	{
		AudioManager.PlaySoundSingle ("Star");
		thirdStars.Play ();
	}

	public void SecondStarsParticle()
	{
		AudioManager.PlaySoundSingle ("Star");
		secondStars.Play ();
	}

	public void FisrtStarsParticle()
	{
		AudioManager.PlaySoundSingle ("Star");
		firstStars.Play ();
	}

	public void StopCounting()
	{
		StartCoroutine( DelayBeforeStop (0.5f));
	}

	IEnumerator DelayBeforeStop (float delay)
	{
		yield return new WaitForSeconds (delay);
		starCountingAud.Stop ();
	}

	public void HighScoreIconAudio()
	{
		AudioManager.PlaySoundSingle ("HighScoreIcon");
	}

	public void HighScoreAudio()
	{
		AudioManager.PlaySoundSingle ("HighScore");
	}

}
