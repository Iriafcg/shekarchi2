﻿using UnityEngine;
using System.Collections;

public class LevelScoreValues : MonoBehaviour {

    #region Properties
    private static LevelScoreValues instance = null;
    
    //public int oneStarScore;
    public int twoStarScore;
    public int threeStarScore;
    

    void Awake()
    {
        instance = this;
    }

    /*public static int OneStarScore
    {
        get
        {
            return instance.oneStarScore;
        }
    }*/

    public static int TwoStarScore
    {
        get
        {
            return instance.twoStarScore;
        }
    }

    public static int ThreeStarScore
    {
        get
        {
            return instance.threeStarScore;
        }
    }

    #endregion
}
