﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelUnlocker : MonoBehaviour {

    public Sprite unlockedBG;

	string levelName;
    Transform myTrans;
	SpriteRenderer spriteRenderer;
	new BoxCollider collider;
    TextMesh txtName;
	Animation loading;
    SpriteRenderer[] stars = new SpriteRenderer[3];
	int currentLevel;
	public GameObject skipObj;

    void Awake()
    {
	    if (PlayerPrefs.HasKey($"skip{gameObject.name}"))
	    {
		    skipObj.SetActive(true);
	    }
	    
        levelName = name;
        myTrans = transform;
		currentLevel = PlayerPrefs.GetInt("currentLevel", 1);
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider>();
        txtName = myTrans.GetChild(0).GetComponent<TextMesh>();
		loading = Camera.main.transform.GetChild (0).GetComponent<Animation> ();
        for (int i = 0; i < stars.Length; i++)
        {
            stars[i] = myTrans.GetChild(i + 1).GetComponent<SpriteRenderer>();
        }

		int nameVal;
		int.TryParse(levelName, out nameVal);
		if (nameVal <= currentLevel)
        {
            spriteRenderer.sprite = unlockedBG;
            txtName.text = levelName;
            collider.enabled = true;
        }
        else
        {
            if (PlayerPrefs.HasKey(levelName + "_Stars"))
            {
                spriteRenderer.sprite = unlockedBG;
                txtName.text = levelName;
                collider.enabled = true;                
            }
        }
		
		int starCount = PlayerPrefs.GetInt(levelName + "_Stars", 0);
		for (int i = 0; i < starCount; i++)
		{
			stars[i].enabled = true;
		}
    }

	RaycastHit hit;
	int sceneIndex ;
    void OnMouseUp()
    {
		if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit))
		{
			sceneIndex = -1;
			int.TryParse (hit.collider.GetComponent<LevelUnlocker>().levelName, out sceneIndex);
			if (sceneIndex.ToString() == levelName) 
			{
				AudioManager.PlaySoundSingle ("Click");

				if (1 == sceneIndex)
				{
					if (0 == PlayerPrefs.GetInt("NotFirstTime", 0))
					{
						sceneIndex = SceneManager.sceneCountInBuildSettings - 2; // Intro tooye buildSettings bayad akharin level bashe
						PlayerPrefs.SetInt("NotFirstTime", 1);
						loading.Play ();
						StartCoroutine(Delay(1.5f));
					}
				}

				PlayerPrefs.SetInt ("LastLevel", sceneIndex);
				loading.Play ();
				StartCoroutine(Delay(1.5f));
			}
			else
				spriteRenderer.color = new Color32 (255, 255, 255, 255);
		} 
		else 
		{
			spriteRenderer.color = new Color32 (255, 255, 255, 255);
		}
    }

	void OnMouseDown()
	{
		spriteRenderer.color = new Color32 (255, 255, 255, 150);
	}

	IEnumerator Delay(float delay)
	{
		yield return new WaitForSeconds (delay);
		SceneManager.LoadScene (sceneIndex + 1);
	}
}
