﻿using UnityEngine;
using System.Collections;

public class PageManager : MonoBehaviour {

    public Sprite navigationEnabled, navigationDisabled;
    public SpriteRenderer nextBt, previousBt;
	public int levelsPerPage = 11;
	public int numLevels = 128;
	public PageIndicator pageIndicator;

    static PageManager instance;

    Transform myTrans;
    private int _index;
    int index
    {
	    get => _index;
	    set => _index = Mathf.Clamp(value,0, levelsPerPage);
    }
    
    

    void Awake()
    {
        instance = this;
        myTrans = transform;

        int currentLevel = PlayerPrefs.GetInt("currentLevel", 0);
        
        
        
        index = (currentLevel / levelsPerPage);
        print(index);

        myTrans.localPosition = new Vector3(50 * index, 0, -0.1f);

        CheckNavigation();

		pageIndicator.SetPageNumberAwake(index);
    }

    public static void NextPage()
    {
        instance.index++;
        instance.myTrans.localPosition = new Vector3(50 * instance.index, 0, -0.1f);
        instance.CheckNavigation();
		instance.pageIndicator.SetPageNumber(instance.index, false);
    }

    public static void PreviousPage()
    {
        instance.index--;
        instance.myTrans.localPosition = new Vector3(50 * instance.index, 0, -0.1f);
        instance.CheckNavigation();
		instance.pageIndicator.SetPageNumber(instance.index, true);
    }

    void CheckNavigation()
    {
        if (index == 0)
        {
            previousBt.sprite = navigationDisabled;
            previousBt.GetComponent<BoxCollider2D>().enabled = false;
        }
		else if (index == (numLevels / levelsPerPage))
        {
            nextBt.sprite = navigationDisabled;
            nextBt.GetComponent<BoxCollider2D>().enabled = false;
        }
        else
        {
            previousBt.sprite = nextBt.sprite = navigationEnabled;
			previousBt.GetComponent<BoxCollider2D>().enabled = true;
			nextBt.GetComponent<BoxCollider2D>().enabled = true;
        }        
    }


	bool maySwipe = false;

	int minSwipeDistance = 100;
	float minSwipeTime = .09f;
	float maxSwipeTime = 2f;
	float swipeBeginTime;
	Vector3 swipeOrigin;
	Vector3 swipeEnd;

	void Update()
	{
		ProcessSwipe();	
	}

	void ProcessSwipe()
	{
		if (1 == Input.touchCount)
		{
			switch(Input.GetTouch(0).phase)
			{
			case TouchPhase.Began:
				{
					maySwipe = true;
					swipeOrigin = Input.GetTouch(0).position;
					swipeBeginTime = Time.time;
					break;
				}
			case TouchPhase.Ended:
				{
					if (maySwipe)
					{
						swipeEnd = Input.GetTouch(0).position;
						DoSwipe();
					}
					maySwipe = false;
					break;
				}
			}
		}
	}

	void DoSwipe ()
	{
		var distance = swipeOrigin.x - swipeEnd.x;
		if (Mathf.Abs(distance) > minSwipeDistance && (Time.time - swipeBeginTime > minSwipeTime) && (Time.time - swipeBeginTime < maxSwipeTime))
		{
			if (distance > 0)
			{
				if (index < numLevels / levelsPerPage)
				{
					NextPage ();
					AudioManager.PlaySoundSingle("Click");
				}
			}
			else if (distance < 0)
			{
				if (index > 0)
				{
					PreviousPage ();
					AudioManager.PlaySoundSingle("Click");
				}
			}
		}
	}
}
