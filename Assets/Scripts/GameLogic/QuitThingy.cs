﻿using UnityEngine;
using System.Collections;

public class QuitThingy : MonoBehaviour {

	Animation anim;
	bool showing = false;

	void Awake()
	{
		anim = GetComponent<Animation>();
	}

	void ShowQuitDialog()
	{
		anim ["Gameover"].speed = 1;
		anim ["Gameover"].time = 0;
		anim.Play();
		Fade.FadeIn ();
		showing = !showing;
	}

	public void HideQuitDialog()
	{
		anim ["Gameover"].speed = -1;
		anim ["Gameover"].time = anim ["Gameover"].length;
		anim.Play();
		Fade.FadeOut ();
		showing = !showing;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (showing)
				HideQuitDialog ();
			else
				ShowQuitDialog ();
		}
	}
}
