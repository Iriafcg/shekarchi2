﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RecordLoader : MonoBehaviour {

	void Awake () 
	{
		Text txtRecord = transform.GetComponent<Text>();
		var levelRecord = (SceneManager.GetActiveScene ().buildIndex - 1) + "_Record";
		int record = PlayerPrefs.GetInt(levelRecord, 0);
		txtRecord.text =record.ToString () +  " :ﺩﺭﻮﻛﺭ"; 
		Image[] stars = transform.GetComponentsInChildren<Image>();
		int recordStars = PlayerPrefs.GetInt ((SceneManager.GetActiveScene ().buildIndex - 1) + "_Stars", 0);
		for (int i = 3; i < 3 + recordStars; i++)
			stars [i].color = new Color32 (255, 255, 255, 255);
	}
}
