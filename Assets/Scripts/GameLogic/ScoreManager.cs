﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    static ScoreManager instance;

    Text txtScore;
    Animation scoreAnim;
    int score;
	float theTime;
	int oldScore;

    public static int Score
    {
        get
        {
            return instance.score;
        }

        set
        {
			instance.oldScore = instance.score;
			instance.score = value;
            //instance.txtScore.text = instance.score.ToString();
			if (instance.scoreAnim != null)
				instance.scoreAnim.Play();
			instance.theTime = Time.time + 0.5f;
        }
    }

	void Awake () 
    {
        instance = this;
        txtScore = transform.GetComponent<Text>();
        scoreAnim = GetComponent<Animation>();
        Score = 0;
	}

	void Update ()
	{
		instance.txtScore.text = Mathf.Lerp (instance.oldScore, instance.score, Mathf.InverseLerp (0.5f, 0, instance.theTime - Time.time)).ToString ("0");
	}
}
