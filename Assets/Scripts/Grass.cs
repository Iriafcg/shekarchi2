﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : Flammable
{
    public override void Explode(bool destroy = true,string soundName = "Explosion")
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(myTrans.position, explosionRadius, destroyable);

        foreach (var c in colliders)
        {
                if (c != null && c.GetComponent<Flammable>() )
                {
                    c.GetComponent<Flammable>().StartFlame();
                }
        }


// 	if (hasExploded)
// 			return;
//
// 		hasExploded = true;
//
// 		Collider2D[] colliders = Physics2D.OverlapCircleAll(myTrans.position, explosionRadius, destroyable);
// 		
// 		for (int i = 0; i < colliders.Length; ++i)
// 		{
// 			hit = colliders[i];
// 			print(hit.name);
// 			if (hit.gameObject == this.gameObject)	// TODO: Too intrusive. Maybe think of a better way?
// 				continue;
//
// 			Vector2 dir = (hit.transform.position - myTrans.position);
// 			//float wearoff = 1 - (dir.magnitude / explosionRadius); // damage attenuation factor
// 			//
// 			//			if (wearoff > wearoffThreshold) // age kheyli nazdik bood be noghte enfejar, nabood beshe
// 			//				Destroy(hit.gameObject);
// 			//			else
// 			//			{
// 			//				if (null != hit.gameObject.GetComponent<Attackable>())
// 			//				{
// 			//					hit.GetComponent<Attackable>().ReceiveDamage(hitDamage);
// 			//					hit.GetComponent<Rigidbody2D>().AddForce(dir.normalized * explosionForce * wearoff); // emale niroo
// 			//
// 			//					if (null != hit.gameObject.GetComponent<Masaleh>())
// 			//					{
// 			//						hit.GetComponent<Rigidbody2D>().AddTorque(explosionForce * wearoff / 5); // emale nirooye charkheshi baraye tabi'itar shodane kar
// 			//					}
// 			//				}
// 			//			}
//
// 			float attenuationFactor = Mathf.Exp(-dir.magnitude / explosionRadius);
//
// //			var damageAmount = (dir.magnitude < explosionRadius * .5f) ? hitDamage : .5f * hitDamage;
// //			var force = (dir.magnitude < explosionRadius * .5f) ? explosionForce : .5f * explosionForce;
//
// 			var damageAmount = hitDamage * attenuationFactor;
// 			var force = explosionForce * attenuationFactor;
//
// 			if (null != hit.GetComponent<IDamageable>())
// 			{
// 				hit.GetComponent<IDamageable>().ReceiveDamage(damageAmount);
// 			}
// 			// hit.GetComponent<Rigidbody2D>().AddForce(dir.normalized * force);
// 		}
//
// 		if (PlayerPrefs.GetInt ("VibrateOption", 0) == 1)
// 		#if UNITY_ANDROID
// 			Handheld.Vibrate ();
// 		#endif
//
 		AudioManager.PlaySound (soundName);
// 		
			Destroy(gameObject);
    }
}
