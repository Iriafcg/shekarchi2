﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{

	Animation loading;

	void Awake ()
	{
		loading = Camera.main.transform.GetChild (0).GetComponent<Animation> ();
	}

	public void LoadLevelOne()
	{
		loading.Play ();
		StartCoroutine(Delay(1.5f));
	}

	IEnumerator Delay(float delay)
	{
		yield return new WaitForSeconds (delay);
		SceneManager.LoadScene ("1ok");
	}

	void OnMouseUp()
	{
		LoadLevelOne ();
	}
}
