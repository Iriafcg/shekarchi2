﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointGroup : MonoBehaviour
{
    public enum JointType
    {
        FixedJoint2D,
        SpringJoint2D
    }

    public JointType jointType;

    public int frequently;
    public int damping;
    public float breakForce;

    private Type GetType()
    {
        switch (jointType)
        {
            case JointType.FixedJoint2D:
                return typeof(FixedJoint2D);
                break;
            case JointType.SpringJoint2D:
                return typeof(SpringJoint2D);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void Awake()
    {
        AddJointComponent();
        ConnectBodies();
    }

    private void AddJointComponent()
    {
        var type = GetType();
        for (int i = 1; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.AddComponent(type);
        }
    }

    private void ConnectBodies()
    {
        for (var i = 1; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Joint2D>().connectedBody = transform.GetChild(i-1).GetComponent<Rigidbody2D>();
            transform.GetChild(i).GetComponent<Joint2D>().breakForce = breakForce;
            // transform.GetChild(i).GetComponent<FixedJoint2D>().frequency = frequently;
            // transform.GetChild(i).GetComponent<FixedJoint2D>().dampingRatio = damping;
        }
    }
}
