﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnDestroyEvent : MonoBehaviour
{
    public UnityEvent onDestroyObject;

    private void OnDestroy()
    {
        onDestroyObject.Invoke();
    }
}
