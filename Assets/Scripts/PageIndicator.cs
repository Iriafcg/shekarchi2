﻿using UnityEngine;
using System.Collections;

public class PageIndicator : MonoBehaviour
{
	public Sprite offSprite;
	public Sprite onSprite;
	public SpriteRenderer[] buttons;

	public void SetPageNumber (int index, bool prev)
	{
		if (prev)
		{
			buttons[index + 1].sprite = offSprite;
		}
		else
		{
			buttons[index - 1].sprite = offSprite;
		}
		buttons[index].sprite = onSprite;
	}

	public void SetPageNumberAwake (int index)
	{		
		for (int i = 0; i < buttons.Length; ++i)
		{
			buttons[i].sprite = offSprite;
		}
		buttons[index].sprite = onSprite;
	}
}
