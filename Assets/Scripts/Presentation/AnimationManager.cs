﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class AnimationManager : MonoBehaviour
{ //for calculating our delta time 

    float _timeAtLastFrame = 0F;
    float _timeAtCurrentFrame = 0F;
    float deltaTime = 0F;

    AnimationState _currState;

    bool isPlaying = false;
    //float _startTime = 0F;
    float _accumTime = 0F;

    string animName;

    void Update()
    {
        _timeAtCurrentFrame = Time.realtimeSinceStartup;
        deltaTime = _timeAtCurrentFrame - _timeAtLastFrame;
        _timeAtLastFrame = _timeAtCurrentFrame;

        if (isPlaying) AnimationUpdate();
    }

    void AnimationUpdate()
    {
        //accumulate time
        _accumTime += deltaTime;
        //normalized time is from 0, the animation's beginning to 1, the end. We'll set it where the accumulated time is in the current animation's duration.
        _currState.normalizedTime = _accumTime / _currState.length;
        if (_accumTime >= _currState.length)
        {
            OnAnimationCompleted();
            _currState.enabled = false;
            isPlaying = false;
        }
    }

    public void PlayAnimation(string animationName)
    {
        _accumTime = 0F;
        _currState = GetComponent<Animation>()[animationName];
        _currState.weight = 1;
        _currState.blendMode = AnimationBlendMode.Blend;
        _currState.wrapMode = WrapMode.Once;
        _currState.normalizedTime = 0;
        _currState.enabled = true;
        isPlaying = true;
        animName = animationName;
    }

    internal void OnAnimationCompleted()
    {
        //hook for end of animation
        if (animName == "Unpause")
            Time.timeScale = 1;
    }

	public void AnimationDoneEvent()
	{
	}
}