﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

	public GameObject auidoPlayer;
	public AudioClip[] clips;
	public AudioClip[] singleClips;

    static AudioManager instance;
	AudioSource[] singlePlayers;
	Dictionary<string, AudioClip> singles = new Dictionary<string , AudioClip>();

	Dictionary<string, List<AudioSource>> auds = new Dictionary<string , List<AudioSource>>();

	AudioSource[] allAudioes;

	void Awake () 
    {

		// Debug.LogError ("Uncomment the following line and remove the AudioManager from the Main prefab, also remove the audioManager from intro scene");
		DontDestroyOnLoad (gameObject);

        instance = this;
		singlePlayers = new AudioSource[transform.childCount];
		for (int i =0; i< singlePlayers.Length ; i++)
			singlePlayers[i] = transform.GetChild(i).GetComponent<AudioSource> ();

		List<AudioSource> tempList = new List<AudioSource>();
		int counter = 0;

		for (int i = 0; i < clips.Length; i++) 
		{
			for (int j = 0; j < 5; j++) // baraye har sedaii 5 ta player misazim ta seda ha vasate play shodan ghat nashan 
			{
				GameObject instantiated = Instantiate (auidoPlayer, Vector3.zero, Quaternion.identity) as GameObject;
				instantiated.transform.parent = this.transform;
				instantiated.name = clips [i].name.Substring (0, clips [i].name.Length - 1) + j.ToString();

				tempList.Add(instantiated.GetComponent<AudioSource> ());
				tempList [j + (5 * counter)].clip = clips [i];
			}

			if (i < clips.Length-1 && clips [i].name.Substring (0, clips [i].name.Length - 1) != clips [i+1].name.Substring (0, clips [i+1].name.Length - 1)) 
			{
				auds.Add (clips[i].name.Substring (0, clips [i].name.Length - 1), tempList);
				counter = 0;
				tempList = new List<AudioSource>();
			}
			else
			{
				counter++;
			}
		}

		auds.Add (clips[clips.Length-1].name.Substring (0, clips [clips.Length-1].name.Length - 1), tempList); // clipe akharo dasti add mikonim chon zamane moghayese dakhele for akhari barresi nemishe (bekhatere oon -1)


		for (int i = 0; i < singleClips.Length; i++) 
		{
			singles.Add (singleClips[i].name, singleClips[i]);
		}

		if (PlayerPrefs.GetInt ("AudioOption", 1) == 0)
			SetAudioOption (true);
	}

	public static void PlaySound(string player)
	{
		List<AudioSource> tempAudio;
		if (instance.auds.TryGetValue (player, out tempAudio)) 
		{
			bool played = false;
			for (int i = 0; i < tempAudio.Count; i++) 
			{
				int rnd = Random.Range (0, tempAudio.Count);
				if (tempAudio [rnd] != null && !tempAudio [rnd].isPlaying) 
				{
					tempAudio [rnd].pitch = Random.Range (0.8f, 1.2f);
					tempAudio [rnd].Play ();
					played = true;
				}
				if (played)
					break;

			}
		}
	}

	List<AudioSource> playingAuds = new List<AudioSource>();
	public static void PauseAll()
	{
		instance.allAudioes = GameObject.FindObjectsOfType<AudioSource> ();
		for (int i = 0; i < instance.allAudioes.Length; i++)
			if (instance.allAudioes [i].isPlaying) 
			{
				instance.allAudioes [i].Pause ();
				instance.playingAuds.Add (instance.allAudioes [i]);
			}
	}

	public static void UnpauseAll()
	{
		for (int i = 0; i < instance.playingAuds.Count; i++)
			instance.playingAuds [i].Play();
		instance.playingAuds.Clear ();
	}

	static int singleQueue = 0;
	public static void PlaySoundSingle(string player)
	{
		instance.singlePlayers[singleQueue].clip = instance.singles [player];
		instance.singlePlayers[singleQueue].Play ();
		singleQueue++;
		if (singleQueue == instance.singlePlayers.Length)
			singleQueue = 0;
	}

	public static void SetAudioOption(bool state)
	{
		instance.allAudioes = GameObject.FindObjectsOfType<AudioSource> ();
		for (int i = 0; i < instance.allAudioes.Length; i++)
			instance.allAudioes [i].mute = state;
	}
}
