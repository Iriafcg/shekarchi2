﻿using UnityEngine;
using System.Collections;

public class BGCamera : MonoBehaviour {

	public GameObject environment;
	public GameObject cloudPrefab;

	public float CloudDensity = 0.5f; // 0 .. 1

	Transform myTrans;
	float nextCloudGenerationTime = Mathf.Infinity;

	void Awake () 
	{
		myTrans = transform;
		GameObject ambiance = Instantiate (environment, myTrans.position, Quaternion.identity) as GameObject;
		if (PlayerPrefs.GetInt ("AudioOption", 1) == 0)
			ambiance.GetComponent<AudioSource>().mute = true;

		if (CloudDensity != 0) 
		{
			nextCloudGenerationTime = Time.time + Random.Range (1 / CloudDensity, 2 / CloudDensity);

			int initialCloudCount = Random.Range (1, 5);
			for (int i = 0; i <= initialCloudCount; i++) {
				GameObject cloudInstance = Instantiate (cloudPrefab, new Vector3 (myTrans.position.x + Random.Range (-5, 5), myTrans.position.y + Random.Range (1, 6), 0), Quaternion.identity) as GameObject;
				cloudInstance.GetComponent<Translator> ().speed = Random.Range (0.3f, 1f);
				cloudInstance.transform.localScale = new Vector3 (Random.Range (0.45f, 1f), Random.Range (0.25f, 0.75f), 1);
				nextCloudGenerationTime = Time.time + Random.Range (1 / CloudDensity, 2 / CloudDensity);
			}
		}
	}

	void Update()
	{
		if (Time.time > nextCloudGenerationTime) 
		{
			GameObject cloudInstance = Instantiate (cloudPrefab, new Vector3 (myTrans.position.x - 12, myTrans.position.y + Random.Range (1, 6), 0), Quaternion.identity) as GameObject;
			cloudInstance.GetComponent<Translator>().speed = Random.Range (0.3f, 1f);
			cloudInstance.transform.localScale = new Vector3 (Random.Range (0.45f, 1f), Random.Range (0.25f, 0.75f), 1);
			nextCloudGenerationTime = Time.time + Random.Range (1 / CloudDensity , 2 / CloudDensity);
		}
	}
}
