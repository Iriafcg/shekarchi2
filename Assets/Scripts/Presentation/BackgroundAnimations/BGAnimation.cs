﻿using UnityEngine;
using System.Collections;

public class BGAnimation : MonoBehaviour
{
	public int spawnProbability;
	public float minSpeed;
	public float maxSpeed;
	//private bool flip;

	void Start()
	{
		Debug.Assert (minSpeed <= maxSpeed, "Error: min speed is not smaller than max speed.");
		//flip = Random.Range (0, 2) == 1 ? true : false;
	}

	void EndOfAnimation()
	{
		BGAnimationManager.Instance ().RemoveObject ();
		Destroy (gameObject);
	}
}
