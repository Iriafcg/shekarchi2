﻿using UnityEngine;
using System.Collections;

public class BGAnimationManager : MonoBehaviour {

	private static BGAnimationManager instance;

//	public GameObject[] m_objectPrefabs;
//	public GameObject[] m_dependentObjectPrefabs;
	public GameObject[] m_objects;
	public GameObject[] m_dependentObjects; 
	public int m_maxInstances;
	public float m_spawnInterval;

	private int m_numInstances;
	private float m_lastSpawnCheck;

//	void Awake()
//	{
//		for (int i = 0; i < m_objectPrefabs.Length; ++i)
//		{
//			m_objects[i] = Instantiate(m_objectPrefabs[i]);
//		}
//
//		for (int j = 0; j < m_dependentObjectPrefabs.Length; ++j)
//		{
//			m_dependentObjects[j] = Instantiate(m_dependentObjectPrefabs[j]);
//		}
//	}

	void Start ()
	{
		//transform.position = new Vector3 (transform.position.x, groundMarker.position.y + .7f, transform.position.z);
		instance = this;
		m_lastSpawnCheck = Time.realtimeSinceStartup;
	}
	
	void Update ()
	{
		if (Time.realtimeSinceStartup - m_lastSpawnCheck >= m_spawnInterval)
		{
			m_lastSpawnCheck = Time.realtimeSinceStartup;
			Spawn();
		}
	}

	private GameObject Spawn()
	{
		if (m_numInstances < m_maxInstances)
		{
			int r = Random.Range(0, m_objects.Length);
			//var prob = m_objects[r].GetComponent<BGAnimation>().spawnProbability;
			//var rand = Random.Range(0, 100);
			//if (rand > prob)
			//	return null;
			var a = Random.Range(0, 100);
			var b = Random.Range(0, 100);
			if (a > b)
				return null;
			var obj = m_objects[r];
			var clone = GameObject.Instantiate(obj);
			//var bgAnim = clone.GetComponent<BGAnimation>();
			clone.transform.SetParent(transform);
			clone.transform.localPosition = new Vector3(-15, obj.transform.localPosition.y , Random.Range(obj.transform.localPosition.z - 0.5f , obj.transform.localPosition.z+ 0.5f));
			//clone.SetActive(true);
			//var anim = clone.GetComponent<Animator>();
			//anim.speed = Random.Range (bgAnim.minSpeed, bgAnim.maxSpeed);;

			++m_numInstances;
			return clone;
		}
		else
			return null;
	}

	public GameObject Spawn (int index , Vector3 position)
	{
		if (m_numInstances < m_maxInstances)
		{
			var prob = m_dependentObjects[index].GetComponent<BGAnimation>().spawnProbability;
			var rand = Random.Range(0, 100);
			if (rand > prob)
				return null;

			var clone = GameObject.Instantiate(m_dependentObjects[index]);

			clone.transform.position = new Vector3(-100, 0, 0);
			clone.transform.SetParent(transform);
			clone.SetActive(true);
			++m_numInstances;
			return clone;
		}
		else
			return null;
	}

	public void RemoveObject()
	{
		--m_numInstances;
	}

	public static BGAnimationManager Instance()
	{
		return instance;
	}
}
