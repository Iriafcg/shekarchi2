﻿using UnityEngine;
using System.Collections;

public class BGBomb : BGAnimation
{
	public int explosionIndex;
	
	public void Explode()
	{
		var exp = BGAnimationManager.Instance().Spawn (explosionIndex, transform.position);
		if (null != exp)
		{
			exp.transform.position = transform.position;
		}
	}
}
