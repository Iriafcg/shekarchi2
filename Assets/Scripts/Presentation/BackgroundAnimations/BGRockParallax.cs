﻿using UnityEngine;
using System.Collections;

public class BGRockParallax : MonoBehaviour
{
	void Update ()
	{
		// Move in the same direction as the camera, but **a fraction of** the amount
		var parentPos = transform.parent.parent.position;
		var myPos = transform.position;
		transform.position = new Vector3 (parentPos.x * .5f, myPos.y, myPos.z);

	}
}
