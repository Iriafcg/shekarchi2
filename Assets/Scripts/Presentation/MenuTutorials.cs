﻿using UnityEngine;
using System.Collections;

public class MenuTutorials : MonoBehaviour
{
	public SpriteBasedTutorial[] tutorials;

	SpriteRenderer blackFade;

	Sprite[] images;
	int tutorialIndex;
	int panelIndex = 0;

	int numTuts;
	public bool isPlaying = false;

	public float timeBetweenPanels;
	float nextPanelTime;
	float startTime;

	void Awake ()
	{
		blackFade = transform.GetChild (0).GetComponent<SpriteRenderer> ();
		images = tutorials[0].tutImages;
		//GetComponent<SpriteRenderer> ().sprite = images[0];
	}

	void Start ()
	{
		numTuts = tutorials.Length;
		GetComponent<Collider2D>().enabled = false;
	}
	
	void Update ()
	{
		if (isPlaying && Time.time > nextPanelTime)
		{
			panelIndex = (panelIndex + 1) % images.Length;
			nextPanelTime = Time.time + timeBetweenPanels;
			GetComponent<SpriteRenderer> ().sprite = images[panelIndex];
		}
	}

	public void ShowTutorial(int i)
	{
		isPlaying = true;
		tutorialIndex = i;
		images = tutorials[tutorialIndex].tutImages;
		panelIndex = 0;
		nextPanelTime = Time.time + timeBetweenPanels;
		GetComponent<SpriteRenderer> ().sprite = images[panelIndex];
		GetComponent<Collider2D>().enabled = true;
		blackFade.enabled = true;
	}

	void HideTutorials ()
	{
		isPlaying = false;
		GetComponent<SpriteRenderer> ().sprite = null;
		GetComponent<Collider2D>().enabled = false;
		blackFade.enabled = false;
	}

	void OnMouseUpAsButton ()
	{
		AudioManager.PlaySoundSingle ("Click");
		if (GetComponent<Collider2D>().enabled)
		{
			HideTutorials ();
		}
	}
}
