﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu]

public class SpriteBasedTutorial : ScriptableObject
{
	public Sprite[] tutImages;
}
