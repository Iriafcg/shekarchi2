﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{

	public SpriteBasedTutorial tutorialSprites;

	public float delayBetweenTuts = 1;

	Image tutorialDisplay;

	float nextTutTime;
	float skipAvailableTime;
	int currentTut = 0; 
	int levelIndex;

	void Awake () 
	{		
		levelIndex = SceneManager.GetActiveScene().buildIndex;
		if (1 == PlayerPrefs.GetInt("Tutorial" + levelIndex.ToString(), 0))
		{
			HideTutorial(true);
		}
		tutorialDisplay = GetComponent<Image> ();
		skipAvailableTime = Time.time + (delayBetweenTuts * tutorialSprites.tutImages.Length);
	}
	
	void Update () 
	{
		if (Time.time > nextTutTime) 
		{
			nextTutTime = Time.time + delayBetweenTuts;
			tutorialDisplay.sprite = tutorialSprites.tutImages [currentTut];

			currentTut++;
			if (currentTut == tutorialSprites.tutImages.Length)
				currentTut = 0;
		}
	}

	public void HideTutorial(bool onAwake = false)
	{
		if (onAwake || Time.time > skipAvailableTime) 
		{
			PlayerPrefs.SetInt ("Tutorial" + levelIndex.ToString (), 1);
			gameObject.SetActive (false);
		}
	}
}
