﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Renamer : MonoBehaviour
{
 public int baseNumber;
 private void OnValidate()
 {
  
  for (int i = 0; i < transform.childCount; i++)
  {
   transform.GetChild(i).name = (i + baseNumber).ToString();
  }
 }
}
