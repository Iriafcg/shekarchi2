﻿using System;
using System.Collections;
using System.Collections.Generic;
using RTLTMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Shopping : MonoBehaviour
{
    public RTLTextMeshPro text;
    public float requiredStars;
    public int stars;
    public AirPlane[] airPlanes;
    private void Awake()
    {
        print(SceneManager.sceneCount);

        airPlanes = GetComponentsInChildren<AirPlane>();
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            stars += PlayerPrefs.GetInt( i + "_Stars");
        }

        //stars += 10000;
        PlayerPrefs.SetInt("Stars",stars);

        int selectedAirPlane = PlayerPrefs.GetInt("AirPlane");
        SelectAirPlane(selectedAirPlane);


    }

    public void SelectAirPlane(int index)
    {
        for (int i = 0; i < airPlanes.Length; i++)
        {
            if (i == index)
            {
                airPlanes[i].Highlight();
            }
            else
            {
                airPlanes[i].Check();
            }
        }
    }
}
