using System;
using System.Collections;
using System.Collections.Generic;
using Messaging;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SkipLevel : MonoBehaviour, ISubscriber
{
    public int skipCount;
    public TextMeshProUGUI skipCountText;
    public InGameButtons InGameButtons;
    public Button skipButton;
    private int _defaultSkipCount = 10;
    private string skipKey => $"skip{SceneManager.GetActiveScene().buildIndex - 1}";
    private string starCountKey => (SceneManager.GetActiveScene().buildIndex - 1) + "_Stars";
    private void Awake()
    {
        skipCount = PlayerPrefs.GetInt("skipCount",_defaultSkipCount);
        skipCountText.text = skipCount.ToString();
        if (skipCount <= 0)
        {
            skipButton.interactable = false;
        }
    }

    private void Start()
    {
        SubscribeToEvent ("Win");
    }

    void OnDisable()
    {
        Unsubscribe ("Win");
    }

    public void Next()
    {
        skipCount--;
        
        if (skipCount >= 0)
        {
            SaveData();
            InGameButtons.Next();
        }
    }

    private void SaveData()
    {
        if (!PlayerPrefs.HasKey(skipKey) && !PlayerPrefs.HasKey(starCountKey))
        {
            PlayerPrefs.SetInt("skipCount", skipCount);
            PlayerPrefs.SetString(skipKey, SceneManager.GetActiveScene().name);
            PlayerPrefs.SetInt("currentLevel", SceneManager.GetActiveScene().buildIndex);

            print(SceneManager.GetActiveScene().name);
            skipCountText.text = skipCount.ToString();
        }
    }


    public void Charge()
    {
        if (skipCount < _defaultSkipCount && PlayerPrefs.HasKey(skipKey))
        {
            skipCount++;
            PlayerPrefs.SetInt("skipCount",skipCount);   
            skipCountText.text = skipCount.ToString();
            PlayerPrefs.DeleteKey(skipKey);
        }
    }

    public void SubscribeToEvent (string eventName)
    {
        MessageDispatcher.Instance.Subscribe(this, eventName);
    }

    public void Unsubscribe (string eventName)
    {
        MessageDispatcher.Instance.Unsubscribe(this, eventName);
    }

    public void PushEvent(HavEvent havEvent)
    {
        if (havEvent is GameController.WinEvent)
        {
            Charge();
        }
    }
}
