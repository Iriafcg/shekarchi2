﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpriteButton : MonoBehaviour
{
    public UnityEvent onClick;
    public AudioClip sound;
    private void OnMouseDown()
    {
        onClick.Invoke();
        AudioSource.PlayClipAtPoint(sound,Camera.main.gameObject.transform.position);
    }
}
