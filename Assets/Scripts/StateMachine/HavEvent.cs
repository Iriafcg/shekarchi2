﻿using UnityEngine;
using System.Collections;

public abstract class HavEvent
{
	public abstract string Name{ get; }
}

public class BombReleased : HavEvent
{
	public Transform Target {get; set;}
	
	public BombReleased(Transform target)
	{
		Target = target;
	}

	public override string Name
	{
		get
		{
			return "BombReleased";
		}
	}
}

public class MousePressed : HavEvent
{
	public override string Name
	{
		get
		{
			return "MousePressed";
		}
	}

	public Vector3 Position {get; set;}
	public Vector3 Buttons {get; set;}
	public float PressTime {get; set;}

	public MousePressed(Vector3 position, Vector3 buttons, float time)
	{
		Position = position;
		Buttons = buttons;
		PressTime = time;
	}
}