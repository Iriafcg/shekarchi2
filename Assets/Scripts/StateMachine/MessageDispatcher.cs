﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Messaging
{
	public class MessageDispatcher : MonoBehaviour
	{
		#if DEBUG || DEVELOPMENT_BUILD
		private List<ISubscriber> vipListeners;		// THEY HEAR EVERYTHING
		#endif
		private List<ISubscriber> listeners;
		private Dictionary< string, List<ISubscriber> > subscriptions;
		private List<DelayedEvent> delayedEvents;	// TODO: this should be a queue

		private static MessageDispatcher instance;

		public static MessageDispatcher Instance
		{
			get
			{
				return instance;
			}
		}

		public struct DelayedEvent
		{
			public HavEvent ev;
			public float time;
			public DelayedEvent(HavEvent havEvent, float delay)
			{
				ev = havEvent;
				time = delay;
			}
		}

		private void Awake()
		{
			instance = this;

			subscriptions = new Dictionary<string, List<ISubscriber>> ();
			listeners = new List<ISubscriber> ();
			delayedEvents = new List<DelayedEvent> ();

			#if DEBUG || DEVELOPMENT_BUILD
			vipListeners = new List<ISubscriber>();
			#endif
		}

		void Start ()
		{
		}
		
		void Update ()
		{
			for (int i = 0; i < delayedEvents.Count; ++i)
			{
				if (Time.time >= delayedEvents[i].time)
				{
					DispatchEvent(delayedEvents[i].ev);
					delayedEvents.Remove(delayedEvents[i]);
					if (i == delayedEvents.Count)
						break;
				}
			}
		}

		public void AddListener(ISubscriber listener)
		{
			listeners.Add (listener);
		}

		public void RemoveListener(ISubscriber listener)
		{
			listeners.Remove (listener);
		}

		public void Subscribe(ISubscriber subscriber, string eventName)
		{
			#if DEBUG || DEVELOPMENT_BUILD
			if ("__ALL__" == eventName)
			{
				vipListeners.Add(subscriber);
				return;
			}
			#endif

			if (!subscriptions.ContainsKey (eventName))
				subscriptions [eventName] = new List<ISubscriber> ();
			subscriptions [eventName].Add (subscriber);
		}

		public void Unsubscribe(ISubscriber subscriber, string eventName)
		{
			subscriptions [eventName].Remove (subscriber);
		}

		public void DispatchEvent(HavEvent havEvent, float delay = -1)
		{
			if (delay > 0)
			{
				delayedEvents.Add(new DelayedEvent(havEvent, Time.time + delay));
				return;
			}

			#if DEBUG || DEVELOPMENT_BUILD
			for (int i = 0; i < vipListeners.Count; ++i)
			{
				vipListeners[i].PushEvent (havEvent);
			}
			#endif

			if (subscriptions.ContainsKey(havEvent.Name))
			{
				List<ISubscriber> subscribers = subscriptions [havEvent.Name];
				for (int i = 0; i < subscribers.Count; ++i)
				{
					subscribers [i].PushEvent (havEvent);
				}
			}
		}

	}
}