﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Automata
{
	public abstract class State
	{
		protected StateMachine stateMachine = null;
		//private bool isActive = false;

		private List<State> subStates;

		public State(StateMachine owner)
		{
			stateMachine = owner;
			Name = this.ToString();
		}

		public State(StateMachine owner, string name)
		{
			stateMachine = owner;
			Name = name;
		}

		public State()
		{
			Name = this.ToString();
		}

		public string Name { get; set; }


		public void SetStateMachine(StateMachine stateMachine)
		{
			this.stateMachine = stateMachine;
		}

		public virtual void OnEnterEarly(HavEvent havEvent) {}
		public virtual void OnLeaveEarly(HavEvent havEvent)	{}
		public virtual void OnEnter (HavEvent havEvent)	{}
		public virtual void OnLeave (HavEvent havEvent)	{}
		public virtual void OnEnterLate (HavEvent havEvent) {}
		public virtual void OnLeaveLate (HavEvent havEvent)	{}

		public StateMachine GetStateMachine()
		{
			return stateMachine;
		}

	//	protected void DispatchEvent (string eventName, Object parameters = null /*, string receiver*/)
	//	{
	//		stateMachine.DispatchEvent (eventName, parameters);
	//	}

		protected void DispatchEvent (HavEvent havEvent, float delay = -1)
		{
			stateMachine.DispatchEvent (havEvent, delay);
		}

		protected void PushAndDispatchEvent (HavEvent havEvent, float delay = -1)
		{
			stateMachine.PushEvent(havEvent);
			stateMachine.DispatchEvent (havEvent, delay);
		}

		protected void PushEvent(HavEvent havEvent)
		{
			stateMachine.PushEvent(havEvent);
		}

		public virtual void Update () {}
		public virtual void LateUpdate () {}
		public virtual void FixedUpdate () {}

	//	public virtual void Update ()
	//	{
	//		for (int i = 0; i < subStates.Count; ++i)
	//		{
	//			if (subStates [i].isActive)
	//			subStates [i].Update ();
	//		}
	//	}

	}
}