﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Messaging;

namespace Automata
{
	public class StateMachine : MonoBehaviour, ISubscriber
	{
		public struct Transition
		{
			public State fromState;
			public State toState;
			public string eventName;
			public Transition(State fromState, State toState, string eventName)
			{
				this.fromState = fromState;
				this.toState = toState;
				this.eventName = eventName;
			}
		}

		#if DEBUG || DEVELOPMENT_BUILD
		public State currentState;
		public State nextState;
		#else
		protected State currentState;
		protected State nextState;
		#endif
		public State previousState;
		public bool isLogging = false;

		private State initialState;
		List<State> states;
		protected List<Transition> transitions;

		// FIXME: This should be private
		protected Queue<HavEvent> eventQueue;

		protected State InitialState
		{
			get
			{
				return initialState;
			}
			set
			{
				currentState = initialState = value;
			}
			
		}

		protected virtual void Awake()
		{
			eventQueue = new Queue<HavEvent> ();
			transitions = new List<Transition> ();
		}

		protected virtual void Start ()
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			MessageDispatcher.Instance.AddListener (this);
			currentState = initialState;
			initialState.OnEnterEarly (null);
			initialState.OnEnter (null);
			initialState.OnEnterLate (null);
		}

		public Transition AddTransition(State fromState, State toState, string eventName)
		{
			var ret = new Transition (fromState, toState, eventName);
			transitions.Add (ret);
			return ret;
		}

		public void RemoveTransition(Transition transition)
		{
			transitions.Remove (transition);
		}

		public virtual void PushEvent(HavEvent havEvent)
		{
			//eventQueue.Enqueue(havEvent);
			OnEvent(havEvent);
		}

		public virtual void Update ()
		{
			currentState.Update ();

	//		TODO: A queue is more elegant. MAKE IT WORK
	//		foreach (var ev in eventQueue)
	//		{
	//			var eventParamPair = eventQueue.Dequeue ();
	//			OnEvent (eventParamPair);
	//		}
		}

		void OnEvent(HavEvent havEvent)
		{
			string eventName = havEvent.Name;

			for (int i = 0; i < transitions.Count; ++i)
			{
				if (transitions [i].fromState == currentState && transitions [i].eventName == eventName)
				{
					#if UNITY_EDITOR || UNITY_EDITOR_64
					if (isLogging)
					{
						Debug.Log("<color=red>" + currentState.Name + "</color> <color=cyan>" + havEvent.Name + "</color>");
					}
					#endif

					previousState = currentState;
					nextState = transitions [i].toState;

					previousState.OnLeaveEarly (havEvent);
					nextState.OnEnterEarly (havEvent);
					previousState.OnLeave(havEvent);
					nextState.OnEnter(havEvent);
					previousState.OnLeaveLate(havEvent);
					nextState.OnEnterLate(havEvent);

					currentState = nextState;

					#if UNITY_EDITOR || UNITY_EDITOR_64
					if (isLogging)
					{
						Debug.Log("<color=green>" + currentState.Name + "</color>");
					}
					#endif

					break;
				}
			}
		}

		public void AddState(State state)
		{
			state.SetStateMachine (this);
		}

		public void SubscribeToEvent(string eventName)
		{
			MessageDispatcher.Instance.Subscribe (this, eventName);	
		}

	//	public void DispatchEvent (string eventName, Object parameters = null)
	//	{
	//		MessageDispatcher.Instance.DispatchEvent (eventName, parameters);
	//	}

		public void DispatchEvent (HavEvent havEvent, float delay = -1)
		{
			MessageDispatcher.Instance.DispatchEvent (havEvent, delay);
		}

		public void Unsubscribe (string eventName)
		{
			MessageDispatcher.Instance.Unsubscribe(this, eventName);
		}
	}
}