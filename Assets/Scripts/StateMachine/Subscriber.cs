﻿using UnityEngine;
using System.Collections;

namespace Messaging
{
	public interface ISubscriber
	{
		void SubscribeToEvent (string eventName);
		void Unsubscribe (string eventName);
		void PushEvent (HavEvent havEvent);
	}
}