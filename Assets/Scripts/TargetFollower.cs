﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFollower : MonoBehaviour
{
    public Transform target;

    void FixedUpdate()
    {
        // transform.rotation = target.rotation;
        transform.position = new Vector3(target.position.x,target.position.y,target.position.z);
    }
}
