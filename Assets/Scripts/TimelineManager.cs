﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour
{
    private PlayableDirector timeline;
    private bool _isPlaying;

    private void Awake()
    {
        timeline = GetComponent<PlayableDirector>();
        timeline.Evaluate();
    }


    public void PlayAnimation()
    {
        if(_isPlaying) return;
        StartCoroutine(Play());   

        
    }

    public void ReverseAnimation()
    {
        if(_isPlaying) return;
        StartCoroutine(Reverse());   
    }


    IEnumerator Play()
    {
        _isPlaying = true;
        
        float dt = 0;
        while (dt < timeline.duration)
        {
            dt += Time.deltaTime / (float) timeline.duration;

            timeline.time = Mathf.Max(dt, 0);
            timeline.Evaluate();
            yield return null;
        }

        _isPlaying = false;
    }

    private IEnumerator Reverse()
    {
        _isPlaying = true;

        float dt = (float) timeline.duration;

        while (dt > 0)
        {
            dt -= Time.deltaTime / (float) timeline.duration;

            timeline.time = Mathf.Max(dt, 0);
            timeline.Evaluate();
            yield return null;
        }
        
        _isPlaying = false;
    }
}