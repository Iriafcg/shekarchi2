﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailFade : MonoBehaviour
{
    private TrailRenderer _trailRenderer;
    public Color color;
    public float fadeSpeed = 0.5f;
    private void Awake()
    {
        _trailRenderer = GetComponent<TrailRenderer>();
        color = Color.white;
    }

    void Update()
    {
        color.a -= Time.deltaTime * fadeSpeed;
        _trailRenderer.material.SetColor("_TintColor",color);
    }
}
