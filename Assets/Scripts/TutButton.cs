﻿using UnityEngine;
using System.Collections;

public class TutButton : MonoBehaviour
{

	public int index;
	public MenuTutorials tuts;

	void Awake ()
	{
		tuts = transform.parent.gameObject.GetComponent<MenuTutorials> ();
	}

	void OnMouseUpAsButton ()
	{
		AudioManager.PlaySoundSingle ("Click");
		if (!tuts.isPlaying)
			tuts.ShowTutorial (index);
	}
}
