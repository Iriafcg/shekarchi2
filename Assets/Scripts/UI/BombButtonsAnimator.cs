﻿using UnityEngine;
using System.Collections;

public class BombButtonsAnimator : MonoBehaviour
{
	private Animator animator;

	private void Awake()
	{
//		InGameButtons.OnPause += ButtonsOut;
//		InGameButtons.OnUnPause += ButtonsIn;
//
//		Bombs.OnBombEquipped += ButtonsOut;
//		Jet.OnNextTurn += ButtonsIn;
//
//		GameController.OnLevelComplete += KeepMenuOffScreen;
//		GameController.OnGameOver += KeepMenuOffScreen;
	}

	private void Start()
	{
		animator = gameObject.GetComponent<Animator> ();
		ButtonsIn ();
	}

	public void ButtonsIn()
	{
		animator.SetInteger ("state", 0);
	}

	public void ButtonsOut()
	{
		animator.SetInteger ("state", 1);
	}

	void OnDestroy()
	{
//		InGameButtons.OnPause -= ButtonsOut;
//		InGameButtons.OnUnPause -= ButtonsIn;
//
//		Bombs.OnBombEquipped -= ButtonsOut;
//		Jet.OnNextTurn -= ButtonsIn;
//
//		GameController.OnLevelComplete -= KeepMenuOffScreen;
//		GameController.OnGameOver -= KeepMenuOffScreen;
	}

	void KeepMenuOffScreen()
	{
//		Jet.OnNextTurn -= ButtonsIn;
	}

	public void SwipSound()
	{
		AudioManager.PlaySoundSingle ("Swip");
	}
}
