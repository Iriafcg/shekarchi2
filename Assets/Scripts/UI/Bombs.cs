﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Messaging;

public class Bombs : MonoBehaviour , ISubscriber {

	public Image selectedBomb;
    public Button[] bombButtons = new Button[3];
	public GameObject[] bombPrefabs = new GameObject[3];
	public bool[] showTrajectory; // = new bool[3];

	Image[] bombTypes = new Image[3];
	int equippedBombIndex = -1;
	Transform bombHatch;

    static Bombs instance;
    IShootable[] bombs = new IShootable[3];
	GameObject[] bombInstances = new GameObject[3];
    bool[] usedBombs = {false , false, false};
	[HideInInspector]
	public int bombsLeft = 3;

	void Awake () 
    {
        instance = this;
    
		bombHatch = GameObject.FindGameObjectWithTag("BombSpawnPoint").transform;

	    for (int i = 0; i < bombs.Length; i++)
        {
			bombInstances[i] = GameObject.Instantiate(bombPrefabs[i]);
			bombInstances [i].transform.parent = transform;
			bombInstances [i].transform.localPosition = new Vector3 (0, i * 5, 0);
			bombs[i] = bombInstances[i].GetComponent<IShootable>();

			bombTypes[i] = bombButtons[i].GetComponent<Image>();
			bombTypes[i].sprite = bombs[i].ButtonUI;
        }
	}

	void Start()
	{
		if (0 == showTrajectory.Length)
		{
			showTrajectory = new bool[3];
			for (int i = 0; i < 3; ++i)
			{
				showTrajectory[i] = false;
			}
		}

		SubscribeToEvent ("Win");
		SubscribeToEvent ("Fail");
	}

	void OnDisable()
	{
		Unsubscribe ("Win");
		Unsubscribe ("Fail");
	}


	public static int BombsLeft
	{
		get
		{
			return instance.bombsLeft;
		}
	}

	public void DisableButtons()
	{
		for (int i = 0; i < 3; ++i)
		{
			bombButtons[i].interactable = false;
			bombTypes[i].color = new Color32(255, 255, 255, 155);
		}
	}

	public void EnableButtons()
	{
		for (int i = 0; i < 3; ++i)
		{
			instance.bombButtons[i].interactable = !instance.usedBombs[i];
			if (instance.usedBombs[i] == false)
				instance.bombTypes[i].color = new Color32(255, 255, 255, 255);			
		}
	}

	public void EquipBomb(int index)
	{
		equippedBombIndex = index;
		var bombObject = bombs[equippedBombIndex] as IShootable;
		bombObject.Equip(showTrajectory[index]);
		usedBombs[equippedBombIndex] = true;
		bombsLeft--;
		selectedBomb.sprite = bombTypes [index].sprite;
		MessageDispatcher.Instance.DispatchEvent (new BombEquippedEvent());
		AudioManager.PlaySoundSingle ("Click");
	}

	public Transform ReleaseEquipped()
    {
		Transform ret = null;
		if (equippedBombIndex == -1)
			return null;
		bombButtons[equippedBombIndex].interactable = false;
		var bombObject = bombs[equippedBombIndex] as MonoBehaviour;
		bombObject.enabled = true;
		bombObject.gameObject.SetActive(true);
		bombObject.transform.position = bombHatch.position;
		bombs[equippedBombIndex].Shoot();
		ret = bombs[equippedBombIndex].Position;
		equippedBombIndex = -1;
		return ret;
    }

    public static Bombs Instance { get { return instance; } }

	#region Events

	public class BombEquippedEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "BombEquipped";
			}
		}
	}

	#endregion

	#region ISubscriber implementation

	public void SubscribeToEvent (string eventName)
	{
		MessageDispatcher.Instance.Subscribe(this, eventName);
	}

	public void Unsubscribe (string eventName)
	{
		MessageDispatcher.Instance.Unsubscribe(this, eventName);
	}

	public void PushEvent (HavEvent havEvent)
	{
		if (havEvent is GameController.WinEvent || havEvent is GameController.FailEvent)
			selectedBomb.enabled = false;
	}

	#endregion
}
