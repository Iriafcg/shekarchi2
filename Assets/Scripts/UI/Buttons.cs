﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour {

	string buttonName;

	void Awake () 
    {
        buttonName = name;
        GetComponent<Animation>()["ButtonIdle"].time = Random.Range(0, 2);
	}
	
	void OnMouseUp () 
    {
		AudioManager.PlaySoundSingle ("Click");
		switch (buttonName) 
		{
		case "StartBt":
			MenuButtons.StartBt ();
			break;

		case "CreditsBt":
			MenuButtons.CreditsBt ();
			break;

		case "SettingsBt":
			MenuButtons.SettingsBt ();
			break;

		case "TutorialsBt":
			MenuButtons.TutorialsBt ();
			break;

		case "BackBt":
			MenuButtons.BackBt ();
			break;

		case "NextBt":
			PageManager.NextPage ();
			break;

		case "PreviousBt":
			PageManager.PreviousPage ();
			break;

		case "SkipBt":
			SceneManager.LoadScene("01");
			break;

		case "AudioBt":
			MenuButtons.AudioBt ();
			transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = !transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled;
			break;

		case "VibrateBt":
			MenuButtons.VibrateBt ();
			transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = !transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled;
			break;

		}
	}
}
