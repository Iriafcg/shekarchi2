﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Fade : MonoBehaviour {

	static Image fade;

	void Awake () 
	{
		fade = GetComponent<Image> ();
	}

	public static void FadeIn()
	{
		fade.color = new Color32 (255, 255, 255, 150);
	}

	public static void FadeOut()
	{
		fade.color = new Color32 (255, 255, 255, 0);
	}
}
