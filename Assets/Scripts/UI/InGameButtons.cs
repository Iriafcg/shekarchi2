﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Messaging;

public class InGameButtons : MonoBehaviour
{
	public Button pauseButton;
	public Image unpauseButton;

    bool isPaused = false;	// TODO: this sript has only two states, so a boolean works as well as a state machine for now

	float logTime;
	float logStep = 2.0f;

	public static InGameButtons instance;

	public void DisableButtons()
	{
		pauseButton.interactable = false;
	}

    #region pause_menu 
    public void Pause()
    {
		Debug.Log ("UI PAUSE CLICKED");
		ProcessPause ();
    }

    public void Reset()
    {
        Time.timeScale = 1;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		SaveManualZoom();
    }

	public void Quit()
	{
		Application.Quit ();
	}

	public void CancelQuit()
	{

	}

    public void Menu()
    {
		ResetManualZoom();
		Time.timeScale = 1;
        SceneManager.LoadScene ("Menu");
    }

    public void Next ()
    {

		// if ((SceneManager.sceneCount - 1) != SceneManager.GetActiveScene ().buildIndex)
		// {
			Time.timeScale = 1;
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		// }
		ResetManualZoom();
    }

    #endregion

	public void ProcessPause ()
	{
		if (isPaused)
		{
			DoUnPause ();
		}
		else
		{
			DoPause ();
		}
		Debug.Log (isPaused.ToString().ToUpper());
	}

	public void DoUnPause()
	{
		Debug.Log ("CHECKING UNPAUSE");
		if (isPaused)
		{
			unpauseButton.enabled = false;
			isPaused = false;
			Debug.Log ("UNPAUSING");
			EventManager.instance.unPaused ();
			MessageDispatcher.Instance.DispatchEvent(new UnpauseEvent());
		}
	}

	public void DoPause()
	{
		isPaused = true;
		unpauseButton.enabled = true;
		Debug.Log ("PAUSING");
		EventManager.instance.Paused ();
		MessageDispatcher.Instance.DispatchEvent(new PauseEvent());
	}

	void SaveManualZoom()
	{
		PlayerPrefs.SetInt ("LastLevel", SceneManager.GetActiveScene ().buildIndex);
		PlayerPrefs.SetFloat ("ManualZoom", Camera.main.orthographicSize);
	}

	void ResetManualZoom()
	{
		PlayerPrefs.SetFloat ("ManualZoom", -1f);
	}


	void Awake ()
	{
		instance = this;
	}

	void Start ()
	{
		logTime = Time.realtimeSinceStartup + logStep;
	}

    void Update()
    {
//        if (Input.GetKeyDown(KeyCode.Escape))
//        {
//			DoPause ();
//        }
		if (Time.realtimeSinceStartup > logTime)
		{
			Debug.Log(unpauseButton.enabled);
			logTime = Time.realtimeSinceStartup + logStep;
		}
    }

	#region Events

	class PauseEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "Pause";
			}
		}
	}

	class UnpauseEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "Unpause";
			}
		}
	}

	#endregion
}
