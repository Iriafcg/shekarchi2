﻿using UnityEngine;
using System.Collections;

public class Marker : MonoBehaviour {
	
	[SerializeField]
	Texture2D iconImage = null;

	void OnDrawGizmos ()
	{
		//Debug.Log (iconImage.name);
		Gizmos.DrawIcon (transform.position, iconImage.name, true);
		//Gizmos.DrawSphere(transform.position, 1);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
