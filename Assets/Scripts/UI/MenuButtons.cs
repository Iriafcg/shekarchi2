﻿using UnityEngine;
using System.Collections;

public class MenuButtons : MonoBehaviour
{

    static MenuButtons instance;

	Animation cam;

	string lastPlayedClipName;

	void Awake () 
    {
        instance = this;
		cam = Camera.main.GetComponent<Animation>();

		if (PlayerPrefs.GetInt ("LastLevel", 0) > 0)
			StartBt ();
	}
	
	public static void StartBt()
    {
		instance.lastPlayedClipName = "GoToLevelSelect";
		instance.PlayAnimation (instance.lastPlayedClipName, false);
    }

    public static void CreditsBt()
    {
		instance.lastPlayedClipName = "GoToCredits";
		instance.PlayAnimation (instance.lastPlayedClipName, false);
    }

    public static void SettingsBt()
    {
		instance.lastPlayedClipName = "GoToSettings";
		instance.PlayAnimation (instance.lastPlayedClipName, false);
    }

	public static void TutorialsBt()
	{
		instance.lastPlayedClipName = "GoToTutorials";
		instance.PlayAnimation (instance.lastPlayedClipName, false);
	}

    public static void BackBt()
    {
		instance.PlayAnimation (instance.lastPlayedClipName, true);
    }

	void PlayAnimation(string clipName, bool backward)
	{
		if (backward) 
		{
			cam [clipName].speed = -1;
			cam [clipName].time = cam [clipName].length;
			cam.Play (clipName);
		} else 
		{
			cam [clipName].speed = 1;
			cam [clipName].time = 0;
			cam.Play (clipName);
		}

	}

	public static void AudioBt()
	{
		int audioOption = PlayerPrefs.GetInt ("AudioOption", 1);
		if (audioOption == 1) 
		{
			PlayerPrefs.SetInt ("AudioOption", 0);
			AudioManager.SetAudioOption (true);
		} else 
		{
			PlayerPrefs.SetInt ("AudioOption", 1);
			AudioManager.SetAudioOption (false);
		}
	}

	public static void VibrateBt()
	{
		int vibrateOption = PlayerPrefs.GetInt ("VibrateOption", 0);
		if (vibrateOption == 1) 
			PlayerPrefs.SetInt ("VibrateOption", 0);
		else
			PlayerPrefs.SetInt ("VibrateOption", 1);
	}
}
