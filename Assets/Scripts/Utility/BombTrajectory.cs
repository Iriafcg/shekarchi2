﻿using UnityEngine;
using System.Collections;

public class BombTrajectory : MonoBehaviour
{
	static bool Show = false;
	static Vector3 velocity;

	public GameObject TrajectoryPointPrefeb;
	private int numTrajectoryPoints = 70;
	private Transform[] trajectoryPoints;
	float distance = 5;    
	Vector3 gravity = new Vector3(0, -1f , 0);
	bool isHiding = false;
	Transform levelBegin;
	Vector3 levelBeginPosition;
	float spriteSize = 3;


	void Start () 
	{
		levelBegin = GameObject.FindGameObjectWithTag("LevelBegin").transform;
		levelBeginPosition = levelBegin.position - Vector3.right * spriteSize;
		trajectoryPoints = new Transform[numTrajectoryPoints];

		for (int i = 0; i < numTrajectoryPoints; i++) 
		{
			var trajectoryPoint = Instantiate (TrajectoryPointPrefeb);
			trajectoryPoint.transform.position = transform.position;
			//trajectoryPoint.transform.parent = transform.parent;
			Transform dot = trajectoryPoint.transform;
			trajectoryPoint.transform.parent = levelBegin;
			//dot.GetComponent<Renderer>().enabled = false;
			trajectoryPoints [i] = dot;
		}

	}

	void Update () 
	{
		if (Show) 
		{
			SetTrajectoryPoints(transform.position , velocity);
		}
		else
		{
			HideTrajectoryPoints();
		}
	}

	void HideTrajectoryPoints()
	{
		if (!isHiding)
		{
			for (int i = 0; i < numTrajectoryPoints; ++i) 
			{
				trajectoryPoints [i].transform.position = levelBeginPosition;
			}
			isHiding = true;
		}	
	}

	public static void Display(bool status, Vector3 vel)
	{
		Show = status;
		velocity = vel;
	}

	void SetTrajectoryPoints(Vector3 pStartPosition , Vector3 pVelocity )
	{
		trajectoryPoints [0].transform.position = pStartPosition;
		Vector3 tmpVel = pVelocity;
		float h = distance / numTrajectoryPoints;

		for (int i = 1; i < numTrajectoryPoints; ++i) 
		{
			trajectoryPoints [i].transform.position = trajectoryPoints [i -1].transform.position + h * tmpVel;
			tmpVel += h * gravity;
		}

		//----------------
//		float velocity = Mathf.Sqrt((pVelocity.x * pVelocity.x) + (pVelocity.y * pVelocity.y));
//		float angle = Mathf.Rad2Deg*(Mathf.Atan2(pVelocity.y , pVelocity.x));
//		float fTime = 0;
//		fTime += 0.1f;
//		for (int i = 0 ; i < numOfTrajectoryPoints ; i++)
//		{
//			float dx = velocity * fTime * Mathf.Cos(angle * Mathf.Deg2Rad);
//			float dy = velocity * fTime * Mathf.Sin(angle * Mathf.Deg2Rad) - (Physics2D.gravity.magnitude * fTime * fTime / 2.0f);
//			Vector3 pos = new Vector3(pStartPosition.x + dx , pStartPosition.y + dy ,2);
//			trajectoryPoints[i].transform.position = pos;
//			trajectoryPoints[i].GetComponent<Renderer>().enabled = true;
//			trajectoryPoints[i].transform.eulerAngles = new Vector3(0,0,Mathf.Atan2(pVelocity.y - (Physics.gravity.magnitude)*fTime,pVelocity.x)*Mathf.Rad2Deg);
//			fTime += 0.1f;
//		}
	}
}
