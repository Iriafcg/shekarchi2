﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

internal class CameraInternalStateMonitor : MonoBehaviour
{	
	internal Text text;
	private static CameraInternalStateMonitor instance = null;
	public static CameraInternalStateMonitor Instance {get{return instance;}}
	private List<string> mystrings;

	void Awake()
	{
		if (null == instance)
			instance = this;
		text = GetComponent<Text>();
		mystrings = new List<string>();
	}

	internal void AddEntry(string text)
	{
		mystrings.Add(text);

		this.text.text = "";

		for (int i = mystrings.Count - 1; i >= 0 && i > mystrings.Count - 11; --i)
		{
			this.text.text += i.ToString() + mystrings[i] + "\n";
		}

	}

	public void Clear()
	{
		mystrings.Clear();
	}
}
