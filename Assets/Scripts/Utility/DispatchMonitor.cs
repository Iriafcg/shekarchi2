﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Messaging;

public class DispatchMonitor : MonoBehaviour, ISubscriber
{
	Text text;

	List<HavEvent> myEvents;

	void Awake()
	{
		myEvents = new List<HavEvent>();
	}

	void Start ()
	{
		text = GetComponent<Text>();
		SubscribeToEvent("__ALL__");
	}

	#region ISubscriber implementation

	public void SubscribeToEvent (string eventName)
	{
		MessageDispatcher.Instance.Subscribe(this, eventName);
	}

	public void Unsubscribe (string eventName)
	{
		MessageDispatcher.Instance.Unsubscribe(this, eventName);
	}

	public void PushEvent (HavEvent havEvent)
	{
		myEvents.Add(havEvent);

		text.text = "";

		for (int i = myEvents.Count - 1; i >= 0 && i > myEvents.Count - 11  ; --i)
		{
			text.text += i.ToString() + myEvents[i].Name + "\n";
		}
	}

	#endregion
}
