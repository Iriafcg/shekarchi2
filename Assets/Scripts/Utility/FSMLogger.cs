﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Automata;

public class FSMLogger : MonoBehaviour {

	public StateMachine cameraFsm;
	Text text;

	// Use this for initialization
	void Start ()
	{
		text = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		#if DEBUG || DEVELOPMENT_BUILD
		text.text = "";
		text.text += cameraFsm.currentState.ToString() + "\n";
		if (null != cameraFsm.previousState)
			text.text += cameraFsm.previousState.ToString() + "\n";		
		#endif
	}
}
