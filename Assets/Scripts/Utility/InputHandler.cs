﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Messaging;

public class InputHandler : MonoBehaviour
{
	private static InputHandler instance;

	public float clickTimeThreshold = .2f;
	public float dragTimeThreshold = .4f;
	public float dragPixelThreshold = 2f;
	public float dragRelativeThreshold = .008f;

	[HideInInspector]
	public MouseButton[] buttons;

	public Vector3 TouchDelta {get; set;}

	private float[] clickWindow = new float[3];
	private float[] dragWindow = new float[3];
	int numButtons;

	#if UNITY_ANDROID
	bool mayClick = false;
	private bool hasDraggedTouch = false;
	private float lastTouchCount = 0;
	#endif

	public struct MouseButton
	{
		public bool wasDownLastFrame;
		public bool wasDraggedLastFrame;
		public Vector3 origin;
	}

	void Awake ()
	{
		buttons = new MouseButton[3];

		for (int i = 0; i < 3; ++i)
		{
			buttons[i].origin = Input.mousePosition;
			buttons[i].wasDownLastFrame = false;
			buttons[i].wasDraggedLastFrame = false;
		}

		instance = this;

		dragPixelThreshold = Screen.width * dragRelativeThreshold;
		Debug.Log ("DragPixelThreshold is : " + dragPixelThreshold);

		#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_STANDALONE
		numButtons = 3;
		#else
		numButtons = 1;
		#endif

	}

	void Start ()
	{		
	}

//	#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_STANDALONE
	void LateUpdate()
	{
		for (int i = 0; i < numButtons; ++i)
		{
			if (Input.GetMouseButtonDown(i))
			{
				buttons[i].wasDownLastFrame = true;
				buttons[i].origin = Input.mousePosition;
				clickWindow[i] = Time.realtimeSinceStartup + clickTimeThreshold;
				dragWindow[i] = Time.realtimeSinceStartup + dragTimeThreshold;
			}
			else if (buttons[i].wasDownLastFrame && Input.GetMouseButton(i) && buttons[i].origin != Input.mousePosition && Time.realtimeSinceStartup >= dragWindow[i]
				&& Mathf.Abs((buttons[i].origin - Input.mousePosition).magnitude) > dragPixelThreshold)
			{
				Debug.Log ("MOUSE_DRAG");
				MessageDispatcher.Instance.DispatchEvent(new MouseDragEvent(i));
				buttons[i].wasDraggedLastFrame = true;
				buttons[i].origin = Input.mousePosition;
			}
			else if (!buttons[i].wasDraggedLastFrame && Input.GetMouseButtonUp(i))				
			{
				if (Time.realtimeSinceStartup >= clickWindow [i] && !InputUtilities.IsPointerOverUIObject ())
				{
					Debug.Log ("MOUSE_CLICK " + i);
					MessageDispatcher.Instance.DispatchEvent (new MouseClickEvent (i));
				}
				buttons[i].wasDownLastFrame = false;

			}
			else if (buttons[i].wasDraggedLastFrame && Input.GetMouseButtonUp(i))
			{
				Debug.Log ("MOUSE_DRAG_DONE");
				MessageDispatcher.Instance.DispatchEvent(new MouseDragDoneEvent(i));
				buttons[i].wasDownLastFrame = false;
				buttons[i].wasDraggedLastFrame = false;
			}
		}			
	}
//	#elif UNITY_ANDROID
	void Update()
	{
		if (1 == Input.touchCount)
		{
			switch (Input.GetTouch(0).phase)
			{
				case TouchPhase.Began:
				{
					buttons[0].wasDownLastFrame = true;
					buttons[0].origin = Input.GetTouch(0).position;
					//clickWindow[0] = Time.realtimeSinceStartup + clickTimeThreshold;
					dragWindow[0] = Time.realtimeSinceStartup + dragTimeThreshold;
					//mayClick = true;
//					Debug.Log ("BEGAN");
					break;
				}
				case TouchPhase.Ended:
				{
					if (mayClick && !hasDraggedTouch && !InputUtilities.IsPointerOverUIObject() /* && Time.realtimeSinceStartup >= clickWindow[0]*/)
					{
						//MessageDispatcher.Instance.DispatchEvent(new MouseClickEvent(0));
					}
					//MessageDispatcher.Instance.DispatchEvent(new TouchDoneEvent());
					hasDraggedTouch = false;
					mayClick = true;
					Debug.Log ("ENDED");
					break;
				}
				case TouchPhase.Moved:
				{
					if (Mathf.Abs (Input.GetTouch(0).deltaPosition.x) > dragPixelThreshold && Time.realtimeSinceStartup >= dragWindow[0])
					{
						TouchDelta = Input.GetTouch(0).deltaPosition;
						MessageDispatcher.Instance.DispatchEvent(new TouchSwipingEvent());
						hasDraggedTouch = true;
					}
					mayClick = false;
					Debug.Log ("MOVED");
					break;
				}
				default:
				{
					Debug.Log (Input.GetTouch(0).phase.ToString().ToUpper());
					break;
				}
			}
			lastTouchCount = 1;
		}
		else if (2 == Input.touchCount)
		{
			mayClick = false;
			// if (TouchPhase.Moved == Input.GetTouch(0).phase || TouchPhase.Moved == Input.GetTouch(1).phase)
			// {
			// MessageDispatcher.Instance.DispatchEvent(new TouchPinchingEvent());
			// }
			// else if (TouchPhase.Ended == Input.GetTouch(0).phase ^ TouchPhase.Ended == Input.GetTouch(1).phase)
			// {
			// 	MessageDispatcher.Instance.DispatchEvent(new TouchDoneEvent());
			// }
			lastTouchCount = 2;
		}		
		else if (0 == Input.touchCount && lastTouchCount != 0)
		{
			mayClick = false;
			// MessageDispatcher.Instance.DispatchEvent(new TouchDoneEvent());
			lastTouchCount = 0;
//			Debug.Log ("COUNT 0");
		}
	}
//	#endif

	public static InputHandler Instance
	{
		get { return instance;}
	}



	#region Events

	public class MouseDragEvent : HavEvent
	{
		public int Index;

		public override string Name
		{
			get
			{
				return "MouseDrag";
			}
		}

		public MouseDragEvent (int index)
		{
			Index = index;
		}
	}

	public class MouseDragDoneEvent : HavEvent
	{
		public int Index;

		public override string Name
		{
			get
			{
				return "MouseDragDone";
			}
		}

		public MouseDragDoneEvent(int index)
		{
			Index = index;
		}
	}

	public class MouseClickEvent : HavEvent
	{
		public int Index;

		public override string Name
		{
			get
			{
				return "MouseClick";
			}
		}

		public MouseClickEvent (int index)
		{
			Index = index;
		}
	}

	public class TouchSwipingEvent : HavEvent
	{
		public override string Name
		{
			get
			{
				return "TouchSwiping";
			}
		}
	}

	// public class TouchPinchingEvent : HavEvent
	// {
	// 	public override string Name
	// 	{
	// 		get
	// 		{
	// 			return "TouchPinching";
	// 		}
	// 	}
	// }

	// public class TouchDoneEvent : HavEvent
	// {
	// 	public override string Name
	// 	{
	// 		get
	// 		{
	// 			return "TouchDone";
	// 		}
	// 	}
	// }

	#endregion
}
