﻿using UnityEngine;
using System.Collections;

public class Oscillate : MonoBehaviour
{
	public float frequency;
	public float phase;
	public float amplitude;

	float initialAngle;

	void Start ()
	{
		initialAngle = transform.rotation.eulerAngles.z;
	}
	
	void Update ()
	{
		transform.rotation = Quaternion.Euler(new Vector3(0, 0, amplitude * Mathf.Cos(frequency * Time.time + phase) + initialAngle));
	}
}
