﻿using UnityEngine;
using System.Collections;

public class SparkRandomSound : MonoBehaviour {

	public float Interval = 10;
	public AudioClip[] sparksClips;

	AudioSource aud;
	ParticleSystem spark;

	float nextSound;

	void Awake () 
	{
		aud = GetComponent<AudioSource>();
		spark = GetComponent<ParticleSystem> ();
		nextSound = Time.time + Random.Range (5, Interval);
	}
	
	void Update () 
	{
		if (Time.time > nextSound) 
		{
			nextSound = Time.time + Random.Range (20f, 20f + Interval);
			aud.clip = sparksClips[ Random.Range (0, sparksClips.Length)];
			aud.Play ();

			spark.Play ();
		}
	}
}
