﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TouchLogger : MonoBehaviour
{
	Text text;

	void Awake()
	{
		text = gameObject.GetComponent<Text>();
	}

	// Use this for initialization
	void Start ()
	{
		text.text = "";	
	}
	
	// Update is called once per frame
	void Update ()
	{
		string txt = "Num tch: " + Input.touchCount + "\n";
		for (int i = 0; i < Input.touchCount; ++i)
		{
			txt += i.ToString() + ", " + Input.GetTouch(i).phase + "\n";
		}
		text.text = txt;
	}
}
