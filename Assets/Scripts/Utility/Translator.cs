﻿using UnityEngine;
using System.Collections;

public class Translator : MonoBehaviour {

	public float speed = 1;
	//public float terminatorX = Mathf.Infinity;

	Transform myTrans;

	void Awake () 
	{
		myTrans = transform;
	}
	
	void Update () 
	{
		myTrans.Translate (Vector3.right * speed * Time.deltaTime);
		//if (myTrans.position.x > terminatorX)
		//	Destroy (gameObject);
	}
}
