﻿using UnityEngine;
using System.Collections;

public class camelRandomSound : MonoBehaviour {

	public float Interval = 5;
	public AudioClip[] zangooleClips;

	AudioSource aud;

	float nextSound;

	void Awake () 
	{
		aud = GetComponent<AudioSource>();
		nextSound = Time.time + Random.Range (2, 5 + Interval);
	}
	
	void Update () 
	{
		if (Time.time > nextSound) 
		{
			nextSound = Time.time + Random.Range (5f, 5 + Interval);
			aud.clip = zangooleClips[ Random.Range (0, zangooleClips.Length)];
			aud.Play ();
		}
	}
}
