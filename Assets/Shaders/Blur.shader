﻿Shader "Custom/Blur" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_cellSize ("Size of blurred pixels", Float) = 1
		_scrWidth ("Screen width", Float) = 640
		_scrHeight ("Screen height", Float) = 480
	}
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
 
			#include "UnityCG.cginc"
 
			uniform sampler2D _MainTex;
			float _scrWidth;
			float _scrHeight;
 			int _cellSize;
 
			float4 frag(v2f_img i) : COLOR
			{
				float2 screen_uv = int2(i.uv * float2(_scrWidth, _scrHeight) / float2(_cellSize, _cellSize)) * int2(_cellSize, _cellSize);
				float2 screenInv = 1 / float2(_scrWidth, _scrHeight);
				
				float2 rp10 = (screen_uv + float2(1, 0)) * screenInv;
				float2 rn10 = (screen_uv + float2(-1, 0)) * screenInv;
				float2 r0p1 = (screen_uv + float2(0, 1)) * screenInv;
				float2 r0n1 = (screen_uv + float2(0, -1)) * screenInv;
				
				float2 rp20 = (screen_uv + float2(2, 0)) * screenInv;
				float2 rn20 = (screen_uv + float2(-2, 0)) * screenInv;
				float2 r0p2 = (screen_uv + float2(0, 2)) * screenInv;
				float2 r0n2 = (screen_uv + float2(0, -2)) * screenInv;
								
				float4 c0 = tex2D(_MainTex, i.uv);
				float4 c1 = tex2D(_MainTex, rp10);
				float4 c2 = tex2D(_MainTex, rn10);
				float4 c3 = tex2D(_MainTex, r0p1);
				float4 c4 = tex2D(_MainTex, r0n1);
				float4 c5 = tex2D(_MainTex, rp20);
				float4 c6 = tex2D(_MainTex, rn20);
				float4 c7 = tex2D(_MainTex, r0p2);
				float4 c8 = tex2D(_MainTex, r0n2);				
				
				float4 c = (c0 + c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8) / 9.0f;

				return c;
			}
			ENDCG
		}
	}
}