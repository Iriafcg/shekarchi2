﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BlurShader : MonoBehaviour {
	
	[SerializeField]
	private Material material = null;
//	private float cellSize;
//	private float lastCheck;

	// Creates a private material used to the effect
	void Awake ()
	{
		//material = new Material( Shader.Find("Hidden/Blur") );
//		cellSize = 1;
//		lastCheck = Time.realtimeSinceStartup;
	}
	
	// Postprocess the image
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
//		if (Time.realtimeSinceStartup - lastCheck >= 2 && cellSize < 4)
//		{
//			lastCheck = Time.realtimeSinceStartup;
//			++cellSize;
//		}

		material.SetFloat("_scrWidth", Screen.width);
		material.SetFloat("_scrHeight", Screen.height);
//		material.SetFloat("_cellSize", cellSize);
		Graphics.Blit (source, destination, material);
	}
}

