Shader "Hidden/EdgeDetect" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_cellSize ("Size of blurred pixels", Float) = 1
		_scrWidth ("Screen width", Float) = 640
		_scrHeight ("Screen height", Float) = 480
	}
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
 
			#include "UnityCG.cginc"
 
			uniform sampler2D _MainTex;
			float _scrWidth;
			float _scrHeight;
 			int _cellSize;
 
			float4 frag(v2f_img i) : COLOR
			{
				float2 screen_uv = int2(i.uv * float2(_scrWidth, _scrHeight) / float2(_cellSize, _cellSize)) * int2(_cellSize, _cellSize);
				float2 screenInv = 1 / float2(_scrWidth, _scrHeight);
				
				float2 right = (screen_uv + float2(1, 0)) * screenInv;
				float2 top = (screen_uv + float2(0, 1)) * screenInv;
				
				
				float4 c0 = tex2D(_MainTex, i.uv);
				float4 c1 = tex2D(_MainTex, right);
				float4 c2 = tex2D(_MainTex, top);
				
				float4 d1 = c1 - c0;
				float4 d2 = c2 - c0;
				float4 c = dot(d1 + d2, d1 + d2);				

				return c;
			}
			ENDCG
		}
	}
}