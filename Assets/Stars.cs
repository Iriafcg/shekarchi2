﻿using System.Collections;
using System.Collections.Generic;
using RTLTMPro;
using UnityEngine;

public class Stars : MonoBehaviour
{
    [SerializeField] private RTLTextMeshPro starsText;
    void Start()
    {
        var stars = PlayerPrefs.GetInt("Stars");
        starsText.text =  "ستاره های شما" + stars + ": ";
    }

}
