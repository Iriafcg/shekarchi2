﻿Shader "Hidden/NewImageEffectShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Hexagon ("Hexagon", 2D) = "white" {}
        _Color("Color",Color) = (1,1,1,1)
        _A("A",Int) = 1
        _B("B",Int) = 1
        

    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _Hexagon;
            fixed4 _Color;
            int _A,_B;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 hexagon = tex2D(_Hexagon, i.uv*_A);

                fixed2 uv = i.uv+_Time.x;
                fixed diagonalUV = uv.x+uv.y;
                fixed lines = 1-frac(diagonalUV*_B);
                fixed4 result = col.a*_Color.a*hexagon*smoothstep(0.1,0.6,lines)*_Color;
                return col+result;
            }
            ENDCG
        }
    }
}
